//
//  CheckInViewController.h
//  SBPoint
//
//  Created by FireOneOne on 5/17/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//


#import "SBFileOperator.h"
#import <CoreLocation/CoreLocation.h>
#import "GoogleLocation.h"

@interface CheckInViewController : UIViewController<CLLocationManagerDelegate,UITextFieldDelegate,GoogleLocationDelegate>
@property (nonatomic,strong) SBFileOperator *operationFile;
- (void)getNowLocation;
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation;

-(void)setLocationAddress:(NSDictionary*)locationAddress;
@end
