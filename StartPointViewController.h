//
//  StartPointViewController.h
//  SBPoint
//
//  Created by FireOneOne on 5/18/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "SBFileOperator.h"
@protocol StartPointDelegate <NSObject>
- (void)refresh;
@end

@interface StartPointViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate>
@property (nonatomic,strong) SBFileOperator *operationFile;
-(void)setPlaceName:(NSString*)place_name;

@property (nonatomic , strong)id <StartPointDelegate>myDelegate;

@end
