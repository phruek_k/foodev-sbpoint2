//
//  EditStaffViewController.m
//  com.voize.SBPoint
//
//  Created by Micheal Carrick on 6/11/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "EditStaffViewController.h"
#import "SBStaff.h"
#import "SBEmployeeType.h"
@interface EditStaffViewController ()
@property (weak, nonatomic) IBOutlet UITextField *staffIdLabel;
@property (weak, nonatomic) IBOutlet UITextField *staffFirstnameLabel;
@property (weak, nonatomic) IBOutlet UITextField *staffLastnameLabel;
@property (weak, nonatomic) IBOutlet UITextField *staffPhoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *staffEmailLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UIButton *getPickerubtn;
@property (weak, nonatomic) IBOutlet UILabel *employeeLabel;
@property (strong, nonatomic)NSArray *arrStatus;
@property (strong, nonatomic)NSString *employee_type;
@property (strong, nonatomic)SBStaff *staffObj;
@property (weak, nonatomic) IBOutlet UILabel *headLabel;
@property (weak, nonatomic) IBOutlet UILabel *employeeTypeHeaderLabel;
@property (strong, nonatomic)NSString *key;

@end

@implementation EditStaffViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)initStyle{
    self.headLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Med" size:20];
    self.employeeTypeHeaderLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Med" size:20];
    
    self.staffIdLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    self.staffFirstnameLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    self.staffLastnameLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    self.staffPhoneLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    self.staffEmailLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    
    UIColor *color = [UIColor colorWithWhite:220/255.0 alpha:1];
    self.staffFirstnameLabel.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"ชื่อ" attributes:@{NSForegroundColorAttributeName: color}];
    self.staffLastnameLabel.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"นามสกุล" attributes:@{NSForegroundColorAttributeName: color}];
    self.staffPhoneLabel.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"เบอร์โทรศัพท์" attributes:@{NSForegroundColorAttributeName: color}];
    self.staffEmailLabel.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"E-mail" attributes:@{NSForegroundColorAttributeName:color}];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    double version = [[[UIDevice currentDevice] systemVersion] doubleValue];
    if(version<=6.1){
        self.navigationController.navigationBar.translucent = YES;
    }
    
    self.staffObj = [[SBStaff alloc] init];
    NSDictionary *staffData = [self.staffObj getStaff];
    
    self.staffIdLabel.text = [staffData objectForKey:@"staff_id"];
    self.staffFirstnameLabel.text = [staffData objectForKey:@"firstname"];
    self.staffLastnameLabel.text = [staffData objectForKey:@"lastname"];
    self.staffPhoneLabel.text = [staffData objectForKey:@"phone"];
    self.staffEmailLabel.text = [staffData objectForKey:@"email"];
    self.employee_type = [staffData objectForKey:@"employee_type"];
    self.key = [staffData objectForKey:@"key"];
    self.picker.hidden = YES;
    
    SBEmployeeType *sb = [[SBEmployeeType alloc]initWithfile];
    self.arrStatus = [sb getArrayEmployeeType];
    [self initStyle];
    
    NSString *employee_type =  [staffData objectForKey:@"employee_type"];
    
    for (int i=0; i<[self.arrStatus count]; i++) {
        if([[self.arrStatus objectAtIndex:i]isEqualToString:[[employee_type uppercaseString] stringByReplacingOccurrencesOfString:@"_" withString:@" "]]){
            self.employeeLabel.text = [[employee_type uppercaseString] stringByReplacingOccurrencesOfString:@"_" withString:@" "];
            [_picker selectRow:i inComponent:0 animated:NO];
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(backToCheckIn:)
                                                 name:@"backToCheckIn"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(backToLogin:)
                                                 name:@"backToLogin"
                                               object:nil];

}

-(IBAction)showPicker:(id)sender{
    self.picker.hidden = NO;
}
-(IBAction)editStaffInformation{
    
    if((self.staffFirstnameLabel.text.length <=0)||(self.staffLastnameLabel.text.length<=0)||(self.staffPhoneLabel.text.length<=0)||(self.staffEmailLabel.text.length<=0)){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                        message: @"กรุณากรอกข้อมูลให้ครบ"
                                                       delegate: nil
                                              cancelButtonTitle: @"ตกลง"
                                              otherButtonTitles: nil
                              ];
        [alert show];
    }else{
        
       
        NSDictionary *staffInformation = [[NSDictionary alloc]initWithObjectsAndKeys:
                                          self.staffIdLabel.text,@"staff_id",
                                          self.staffFirstnameLabel.text,@"firstname",
                                          self.staffLastnameLabel.text,@"lastname",
                                          self.staffPhoneLabel.text,@"phone",
                                          self.staffEmailLabel.text,@"email",
                                          self.employee_type,@"employee_type",
                                          self.key,@"key",
                                          nil];
        [self.staffObj checkBeforeEditStaff:staffInformation];
        
    }
}
-(void)backToCheckIn:(NSNotification*)notification{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
    
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.arrStatus.count;
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.employeeLabel.text = [self.arrStatus objectAtIndex:row];
    self.employee_type = [[self.employeeLabel.text lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    //[_staffObj changeEmployeeType:employee_type];
    [_picker setHidden:TRUE];
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = [self.arrStatus objectAtIndex:row];
    double version = [[[UIDevice currentDevice] systemVersion] doubleValue];
    NSAttributedString *attString;
    if(version<=6.1){
        attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    }else{
        attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    }
    return attString;
    
}

-(void)backToLogin:(NSNotification*)notification{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:vc animated:NO completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}



@end
