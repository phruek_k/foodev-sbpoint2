//
//  HistoryViewController.h
//  SBPoint
//
//  Created by FireOneOne on 5/18/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckInViewController.h"
#import "HistoryCell.h"

@interface HistoryViewController : UITableViewController <UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic) SBFileOperator *operationFile;
@property (nonatomic,strong) NSMutableArray *nameList;
@end
