//
//  HistoryCell.h
//  SBPoint
//
//  Created by FireOneOne on 5/21/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HistoryCellDelegate <NSObject>
- (void) deleteHistoryIndex:(int)row;
@end

@interface HistoryCell : UITableViewCell
@property (weak, nonatomic) id <HistoryCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *staffNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIImageView *alertLabel;
@property (weak, nonatomic) IBOutlet UILabel *staffIdLabel;


@end
