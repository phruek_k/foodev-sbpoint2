//
//  HistoryCell.m
//  SBPoint
//
//  Created by FireOneOne on 5/21/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "HistoryCell.h"

@implementation HistoryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    _placeLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Med" size:18];
    _staffNameLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:20];
    _staffIdLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:13];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
