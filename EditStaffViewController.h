//
//  EditStaffViewController.h
//  com.voize.SBPoint
//
//  Created by Micheal Carrick on 6/11/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditStaffViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>

@end
