//
//  HistoryViewController.m
//  SBPoint
//
//  Created by FireOneOne on 5/18/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "HistoryViewController.h"
#import "SBStaff.h"
#import "SBLocation.h"
#import "SBTrashLocation.h"
@interface HistoryViewController ()<HistoryCellDelegate,UIAlertViewDelegate>
@property(nonatomic)int countSection;
@property(nonatomic,strong)NSMutableDictionary *allSection;
@property(nonatomic,strong)NSMutableArray *dateArray;
@property (strong, nonatomic) IBOutlet UITableView *historyTable;
@property(nonatomic,strong) SBStaff *staffObj;
@property(nonatomic,strong) NSDictionary *staffData;
@property(nonatomic,strong) NSMutableArray *sectionDistance;


@end


@implementation HistoryViewController
int deleteKey;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    _historyTable.delegate = self;
    _historyTable.dataSource = self;
    _historyTable.editing = NO;
    _countSection = 0;
    self.clearsSelectionOnViewWillAppear = NO;
    _operationFile = [[SBFileOperator alloc]initWithName:@"history" :@".txt" :YES];
    _nameList = _operationFile.arrayList;
    _allSection = [[NSMutableDictionary alloc]init];
    _dateArray = [[NSMutableArray alloc]init];
    
    //Set Date to self.dateArray
    for (int i=0; i<[_operationFile.arrayList count]; i++) {
        if([_dateArray count]!=0){
            for(int j=0;j<[_dateArray count];j++){
                NSString *dateSection = [[_operationFile.arrayList[i] objectForKey:@"check_in_time"] substringToIndex:10];
                if([dateSection isEqualToString:_dateArray[j]]){
                    break;
                }
                if(j==[_dateArray count]-1){
                    [_dateArray addObject:[[[_operationFile.arrayList objectAtIndex:i] objectForKey:@"check_in_time"] substringToIndex:10]];
                }
            }
        }else{
            [_dateArray addObject:[[[_operationFile.arrayList objectAtIndex:i] objectForKey:@"check_in_time"] substringToIndex:10]];
        }
    }
    _sectionDistance = [[NSMutableArray alloc]init];
    //Set all checkpoint to _allSection
    for(int i =0 ;i<[_dateArray count];i++){
        
        NSLog(@"Date %@",_dateArray[i]);
         NSMutableArray *tmpFirst = [[NSMutableArray alloc]init];
        for(int j=0;j<[_operationFile.arrayList count];j++){
            if([[[_operationFile.arrayList[j] objectForKey:@"check_in_time"] substringToIndex:10]isEqualToString:_dateArray[i]]){
                [tmpFirst addObject:_operationFile.arrayList[j]];
                if((int)[_sectionDistance count]<=i){
                    int newDis = [[_operationFile.arrayList[j] objectForKey:@"distance"] intValue];
                    _sectionDistance[i] = [NSString stringWithFormat:@"%d",(newDis)];
                }else{
                    int oldDis = [self.sectionDistance[i] intValue];
                    int newDis = [[_operationFile.arrayList[j] objectForKey:@"distance"] intValue];
                    _sectionDistance[i] = [NSString stringWithFormat:@"%d",(newDis+oldDis)];
                }
            }
            if(j==(int)[_operationFile.arrayList count]-1){
                [_allSection setObject:tmpFirst forKey:_dateArray[i]];
            }
            
        }
    }
    
    NSLog(@"section array count :%d",(int)[self.sectionDistance count]);
    
    self.staffObj = [[SBStaff alloc]init];
    self.staffData = [self.staffObj getStaff];
    
    if([_dateArray count]>0){
        _dateArray = [self sortDate:_dateArray];

        for(int i=0;i<(int)[[_allSection objectForKey:_dateArray[0]] count];i++){
            NSLog(@"key id %@",[[_allSection objectForKey:_dateArray[0]][i] objectForKey:@"key_id"]);
        }
        
        //Sort by last time
        for(int i=0;i<[_dateArray count];i++){
            NSMutableArray *dateKey = [_allSection objectForKey:_dateArray[i]];
            
            NSDictionary *tmpDict = [[NSDictionary alloc]init];
            for(int j=0;j<[dateKey count];j++){
                for(int k=j+1;k<[dateKey count];k++){
                  //  NSLog(@"contast %@ = %@",[dateKey[j] objectForKey:@"key_id"],[dateKey[k] objectForKey:@"key_id"]);
                    if([[dateKey[j] objectForKey:@"key_id"] intValue] < [[dateKey[k] objectForKey:@"key_id"] intValue]){
                       // NSLog(@"switch");
                        tmpDict = dateKey[j];
                        dateKey[j] = dateKey[k];
                        dateKey[k] = tmpDict;
                    }
                }
            }
            
            for(int c=0;c<[dateKey count];c++){
               // NSLog(@"sort na %@",[dateKey[c] objectForKey:@"key_id"]);
            }
            
        }
    }

}

- (NSMutableArray*)sortDate:(NSMutableArray*)dateArray{
    for(int i=0;i<dateArray.count;i++){
        NSString *tmp = @"";
        for(int j=i+1;j<dateArray.count;j++){
            NSDate *now = [NSDate date];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"Y-MM-dd";
            [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            NSLog(@"The Current Time is %@",[dateFormatter stringFromDate:now]);
            NSDate *date1 = [dateFormatter dateFromString:dateArray[i]];
            NSDate *date2 = [dateFormatter dateFromString:dateArray[j]];
            if([date1 compare:date2]){
                tmp = dateArray[i];
                dateArray[i] = dateArray[j];
                dateArray[j] = tmp;
            }
        }
    }
    return dateArray;
}

-(void) SynToDel{
    SBTrashLocation *syncTrashFile = [[SBTrashLocation alloc]init];
    [syncTrashFile syncToDeleteToServer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
   
    return _dateArray.count;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
   // [headerSectionView setBackgroundColor:[UIColor colorWithWhite:0.847 alpha:.5f]];
    [tableView setSectionIndexBackgroundColor:[UIColor colorWithWhite:0.847 alpha:.5f]];
    return _dateArray[section];
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    
    UIView *line = [[UIView alloc] initWithFrame
                    :CGRectMake(10,11,tableView.frame.size.width/4-30,1)];
    [line setBackgroundColor:[UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1.0]];
    
    UILabel *label = [[UILabel alloc] initWithFrame
    :CGRectMake(tableView.frame.size.width/4, 2, tableView.frame.size.width/2, 18)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont boldSystemFontOfSize:12]];
    label.font = [UIFont fontWithName:@"DBHelvethaicaX-Med" size:10];
    [label setTextColor:[UIColor colorWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1.0]];
    [label setBackgroundColor:[UIColor colorWithWhite:150/255.0 alpha:0]];
    
    UIView *line2 = [[UIView alloc] initWithFrame
    :CGRectMake((tableView.frame.size.width-tableView.frame.size.width/4)+20,11,tableView.frame.size.width/4-30,1)];
    [line2 setBackgroundColor:[UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1.0]];
    
    NSDateFormatter *dformat = [[NSDateFormatter alloc]init];
    [dformat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dformat dateFromString:[_dateArray objectAtIndex:section]];
    
    NSDateFormatter *newformat = [[NSDateFormatter alloc]init];
    [newformat setDateFormat:@"dd MMMM yyyy"];
   // NSLog(@"header %@",[newformat stringFromDate:date]);
    NSString *string = [NSString stringWithFormat:@"%@   ระยะทาง %d กิโลเมตร",[newformat stringFromDate:date],[_sectionDistance[section] intValue]/1000];
    
    [label setText:string];
    [view addSubview:line];
    [view addSubview:label];
    [view addSubview:line2];
  //  [view setBackgroundColor:[UIColor colorWithRed:166/255.0 green:177/255.0 blue:186/255.0 alpha:1.0]]; //your background color...
    
    return view;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *tmpKey = _dateArray[section];
//  NSLog(@"number of row section %d",(int)[[_allSection objectForKey:tmpKey] count]);
    
    return [[_allSection objectForKey:tmpKey]count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"Section : %d , Row : %d",(int)indexPath.section,(int)indexPath.row);
   HistoryCell *historyCell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
    historyCell.delegate = self;
    historyCell.placeLabel.text = [NSString stringWithFormat:@"%@ > %@",[_allSection[_dateArray[indexPath.section]][indexPath.row] objectForKey:@"start_place"],[_allSection[_dateArray[indexPath.section]][indexPath.row] objectForKey:@"place_name"]];
    
    
    historyCell.staffNameLabel.text = [NSString stringWithFormat:@"%@",[_allSection[_dateArray[indexPath.section]][indexPath.row] objectForKey:@"customer_name"]];
    historyCell.staffIdLabel.text = [NSString stringWithFormat:@"%@",[_allSection[_dateArray[indexPath.section]][indexPath.row] objectForKey:@"tracking_no"]];
    if([[_allSection[_dateArray[indexPath.section]][indexPath.row] objectForKey:@"work_log_id"] isEqualToString:@"null"]){
        [historyCell.alertLabel setHidden:NO];
    }else{
        [historyCell.alertLabel setHidden:YES];
    }
    
    historyCell.deleteButton.tag = [[_allSection[_dateArray[indexPath.section]][indexPath.row] objectForKey:@"key_id"] intValue]+100;
    
    //Edit Seperate line
    if((int)[_allSection[_dateArray[indexPath.section]] count]>indexPath.row+1){
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(70, historyCell.frame.size.height, 160, 1)];
        [separatorLineView setBackgroundColor:[UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1.0]];
        [historyCell.contentView addSubview:separatorLineView];
    }
    return historyCell;
}
-(IBAction)delWorkLogAct:(id)sender{
    UIButton *button = (UIButton *)sender;
    int key_id = (int)button.tag-100;
    deleteKey = key_id;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                    message: @"คุณต้องการลบรายการนี้"
                                                    delegate: self
                                          cancelButtonTitle: @"ยกเลิก"
                                          otherButtonTitles: @"ตกลง"
                                          , nil];
    
    [alert show];
    //[self viewDidAppear:YES];
    NSLog(@"Delete key_id %d",key_id);
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    _operationFile = [[SBFileOperator alloc] initWithName:@"history" :@".txt" :YES];
    if (buttonIndex == 0) {
        NSLog(@"Not Delete!");
        deleteKey = 0;
        
    }else{
        NSLog(@"Delete! %d",deleteKey);
        SBLocation *checkOutPoint = [[SBLocation alloc]init];
        [checkOutPoint directCheckOut];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"stopLocationUrgent" object:nil];
        SBTrashLocation *trash = [[SBTrashLocation alloc]init];
        NSDictionary *locationTodel = [_operationFile getDataByKeyId:[NSString stringWithFormat:@"%d",deleteKey]];
        if(![[locationTodel objectForKey:@"work_log_id"]isEqualToString:@"work_log_id"]){
            [trash setDeleteLocationToTrashFile:[locationTodel objectForKey:@"work_log_id"]];
        }
        [_operationFile removeList:[NSString stringWithFormat:@"%d",deleteKey]];
        [_operationFile arrayParseToJson:_operationFile.arrayList];
        [self viewDidLoad];
        
        [_historyTable reloadData];
        deleteKey = 0;
    }
    
    [self SynToDel];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"\n DidSeclectRow !! at %@",[NSString stringWithFormat:@"%@",[_allSection[_dateArray[indexPath.section]][indexPath.row] objectForKey:@"customer_name"]]);
    NSDictionary *location = [[NSDictionary alloc]initWithObjectsAndKeys
                              :[NSString stringWithFormat:@"%@",[_allSection[_dateArray[indexPath.section]][indexPath.row] objectForKey:@"customer_name"]],@"customer_name"
                              ,[NSString stringWithFormat:@"%@",[_allSection[_dateArray[indexPath.section]][indexPath.row] objectForKey:@"place_name"]],@"place_name"
                              ,[NSString stringWithFormat:@"%@",[_allSection[_dateArray[indexPath.section]][indexPath.row] objectForKey:@"tracking_no"]],@"tracking_no", nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setLocationFromHistory" object:self userInfo:location];
    [self.navigationController popToRootViewControllerAnimated:TRUE];
}


#pragma mark - HistoryViewController Delegate
- (void) deleteHistoryIndex:(int)row{
    [self.operationFile.arrayList removeObjectAtIndex:row];
    [self.operationFile arrayParseToJson:self.operationFile.arrayList];
    [_historyTable reloadData];
    
}



@end
