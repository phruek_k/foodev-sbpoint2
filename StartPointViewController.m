//
//  StartPointViewController.m
//  SBPoint
//
//  Created by FireOneOne on 5/18/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "StartPointViewController.h"
#import "SettingViewController.h"
#import "SBStartPointLocation.h"
@interface StartPointViewController (){
    CLLocationCoordinate2D center;
    CLLocationCoordinate2D tmp;
    MKPointAnnotation *annotation;
}
@property (weak, nonatomic) IBOutlet MKMapView *startPointMap;
@property(strong,nonatomic) CLLocationManager *locationManager;
@property(strong,nonatomic) NSString *lat;
@property(strong,nonatomic) NSString *lng;
@property(strong,nonatomic) NSString *place_name;
@property (weak, nonatomic) IBOutlet UILabel *topicMap;
@property(strong,nonatomic) UILabel *tagLocation;
@end

@implementation StartPointViewController
@synthesize myDelegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _topicMap.font = [UIFont fontWithName:@"DBHelvethaicaX-Med" size:22];

//    [_startPointMap removeAnnotations:_startPointMap.annotations];
//    _startPointMap.delegate = self;
    [_startPointMap removeAnnotations:_startPointMap.annotations];
    _startPointMap.delegate =self;
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = 1;
    [self.locationManager setDelegate:self];
    [self.locationManager startUpdatingLocation];
    
    annotation = nil;
    if(annotation == nil){
        annotation = [[MKPointAnnotation alloc] init];
    }
    NSLog(@"place name is : %@",_place_name);
    
    self.tagLocation = [UILabel alloc];

        

}
-(void)viewDidAppear:(BOOL)animated{
   // nameLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-75Bd" size:24];

}
-(void)setPlaceName:(NSString *)place_name{
    self.place_name = place_name;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    [_locationManager stopUpdatingLocation];
    [self setLocation];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back:(id)sender{
    // dismiss
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - set current Location and change location
-(void)setLocation
{
    center = _locationManager.location.coordinate;
    NSLog(@"Location: %5f %5f",center.latitude,center.longitude);
    tmp = center;
    center.latitude = center.latitude+0.001;
    [self.startPointMap setRegion:MKCoordinateRegionMake(center, MKCoordinateSpanMake(0.005, 0.005)) animated:YES];
    
    annotation.coordinate = tmp;
    [_startPointMap addAnnotation:annotation];
    _lat = [NSString stringWithFormat:@"%.5f",tmp.latitude];
    _lng = [NSString stringWithFormat:@"%.5f",tmp.longitude];
    _startPointMap.delegate = self;
    
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view didChangeDragState:(MKAnnotationViewDragState)newState
   fromOldState:(MKAnnotationViewDragState)oldState{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if (newState == MKAnnotationViewDragStateStarting) {
        [self animateOut];
    }
    if(newState == MKAnnotationViewDragStateEnding){
        _lat = [NSString stringWithFormat:@"%.5f",[view.annotation coordinate].latitude];
        _lng = [NSString stringWithFormat:@"%.5f",[view.annotation coordinate].longitude];
//        [_lat setText:lat];
//        [_lng setText:lng];
        center = [view.annotation coordinate];
        tmp = center;
        center.latitude = center.latitude+0.001;
        [mapView setCenterCoordinate:center animated:YES];
        [view setDragState:MKAnnotationViewDragStateNone];
        [self animateIn];
    }
}

- (IBAction)moveToCurrentLocation:(id)sender
{
    tmp = _locationManager.location.coordinate;
    _lat = [NSString stringWithFormat:@"%.5f",_locationManager.location.coordinate.latitude];
    _lng = [NSString stringWithFormat:@"%.5f",_locationManager.location.coordinate.longitude];
    annotation.coordinate = tmp;
    tmp.latitude = tmp.latitude+0.001;
    [_startPointMap addAnnotation:annotation];
    [_startPointMap setCenterCoordinate:tmp animated:YES];
}

#pragma mark - set custom pin
- (MKAnnotationView *)mapView:(MKMapView *)mv viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *annotationView = [mv dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotationView"];
    if (annotationView == nil) {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"PinAnnotationView"];
    }else {
        annotationView.annotation = annotation;
    }
    annotationView.draggable = YES;
    UIImage *image = [UIImage imageNamed:@"pin.png"];
    //image = [self imageWithImage:image scaledToSize:CGSizeMake(29, 37)];
    annotationView.image = image;
    return annotationView;
}


#pragma mark - animate
-(void) animateOut
{
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
      //  _detailView.alpha = 0;
      //  _consoleAreaView.alpha = 0;
      //  _map.scrollEnabled = YES;
    } completion:^(BOOL finished) {
        if (finished) {
            NSLog(@"finished animateOut");
        }
    }];
}

-(void) animateIn
{
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
       // _detailView.alpha = 1;
       // _consoleAreaView.alpha = 1;
    } completion:^(BOOL finished) {
        if (finished) {
            NSLog(@"finished animateIn");
        }
    }];
}
- (IBAction)savePoint:(id)sender{
    if(![self.lat isEqualToString:@""]&&![self.lng isEqualToString:@""]){
        [self saveStartPoint];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                        message: @"กรุณาตรวจสอบอินเทอร์เน๊ต"
                                                       delegate: nil
                                              cancelButtonTitle: @"OK"
                                            otherButtonTitles: nil];
        [alert show];
    }
}
- (void)saveStartPoint{
    
    SBStartPointLocation *newPlace = [[SBStartPointLocation alloc]initWithPlace:_place_name :_lat :_lng :@"" :@"active" ];
    [newPlace savePlace];
    [self dismissViewControllerAnimated:YES completion:^{
        [myDelegate refresh];
    }];
    
}

#pragma mark - reload
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



@end
