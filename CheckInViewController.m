//
//  CheckInViewController.m
//  SBPoint
//
//  Created by FireOneOne on 5/17/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "CheckInViewController.h"
#import "SBLocation.h"
#import "SBFileOperator.h"
#import "HistoryViewController.h"
#import "SBStaff.h"
#import "SBStartPointLocation.h"
#import "SBEmployeeType.h"



@interface CheckInViewController ()
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIButton *checkInButton;
@property (strong,nonatomic) CLLocation *nowLocation;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *startPointLabel;
@property (strong,nonatomic) NSNumber *current_key_id;
@property (strong,nonatomic) SBLocation *checkOutPoint;
@property (weak, nonatomic) IBOutlet UITextField *placeNameInput;
@property (weak, nonatomic) IBOutlet UITextField *customerNameInput;
@property (weak, nonatomic) IBOutlet UITextField *trackingNoInput;
@property (strong,nonatomic)NSDictionary *staffData;
@property (weak, nonatomic) IBOutlet UILabel *staffNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *staffType;
@property (weak, nonatomic) IBOutlet UILabel *customerInformationLabel;
@property (strong,nonatomic) NSDictionary *startpointDic;
@property (strong,nonatomic) SBStartPointLocation *startPoint;
@property (weak, nonatomic) IBOutlet UIButton *startPointLogo;
@property (weak, nonatomic) IBOutlet UIView *startPointLine;
@property(strong,nonatomic)GoogleLocation *gobj;
@property (weak, nonatomic) IBOutlet UIView *middleLine;
@property (nonatomic)BOOL hasCheckIntoday;
@property (strong, nonatomic)NSArray *arrStatus;

@end

@implementation CheckInViewController
static NSString *History = @"HistoryViewController";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{   
    [super viewDidLoad];
   
    double version = [[[UIDevice currentDevice] systemVersion] doubleValue];
    if(version<=6.1){
        self.navigationController.navigationBar.translucent = YES;
    }

    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    
    [self getNowLocation];
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    _current_key_id = [f numberFromString:[[NSUserDefaults standardUserDefaults] stringForKey:@"current_key_id"]];
    // NSLog(@"=== Current_id = %@ ===",_current_key_id);
    
    [self initStyle];
    
   _operationFile = [[SBFileOperator alloc]initWithName:@"history" :@".txt" :YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopLocationUrgent:)
                                                 name:@"stopLocationUrgent"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setLocationFromHistory:)
                                                 name:@"setLocationFromHistory"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(backToLoginOnCheckIn:)
                                                 name:@"backToLoginOnCheckIn"
                                               object:nil];
    

}
- (void)initStyle{
    _staffNameLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Med" size:23];
    _customerInformationLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Med" size:20];
    
    
    _staffType.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    
    _placeNameInput.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    _customerNameInput.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    _trackingNoInput.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    _locationLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    _startPointLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    
    
    
    //UIColor *color = [UIColor whiteColor];
    UIColor *color = [UIColor colorWithWhite:220/255.0 alpha:1];
    _placeNameInput.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"ชื่อสถานที่" attributes:@{NSForegroundColorAttributeName: color}];
    _customerNameInput.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"ผู้ติดต่อ / ลูกค้า" attributes:@{NSForegroundColorAttributeName: color}];
    _trackingNoInput.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"เลขที่อ้างอิง / tracking no" attributes:@{NSForegroundColorAttributeName: color}];

}
-(void)viewDidAppear:(BOOL)animated{
    self.gobj = [[GoogleLocation alloc]init];
    SBLocation *syncFile = [[SBLocation alloc]init];
    [syncFile checkLocationToSync];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     _operationFile = [[SBFileOperator alloc]initWithName:@"history" :@".txt" :YES];
     self.hasCheckIntoday = [_operationFile hasCheckInToday];
    SBStaff *staffObj = [[SBStaff alloc]init];
    _staffData = [staffObj getStaff];
    _staffNameLabel.text = [NSString stringWithFormat:@"%@ %@",[_staffData objectForKey:@"firstname"],[_staffData objectForKey:@"lastname"]];
    self.startPoint = [[SBStartPointLocation alloc]init];
    _startpointDic = [self.startPoint getPlaceActive];
    _startPointLabel.text = [_startpointDic objectForKey:@"place_name"];
    _staffData = [staffObj getStaff];
    
    //set employee type
    _staffType.text = [[[self.staffData objectForKey:@"employee_type"] uppercaseString] stringByReplacingOccurrencesOfString:@"_" withString:@" "];
   
    
    if([_operationFile hasCheckInToday]){
        NSLog(@"Has check in today : YES");
        [self.startPointLogo setHidden:YES];
        [self.startPointLabel setHidden:YES];
        [self.startPointLine setHidden:YES];
        
        _middleLine.backgroundColor = [[UIColor alloc]initWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1];
        
    }else{
        NSLog(@"Has check in today :NO");
        [self.startPointLogo setHidden:NO];
        [self.startPointLabel setHidden:NO];
        [self.startPointLine setHidden:NO];
        
        _middleLine.backgroundColor = [[UIColor alloc]initWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1];
    }
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)checkIn:(id)sender{
    //[_trackingNoInput.text isEqualToString:@""]
    NSLog(@"Let's Check in");
    SBEmployeeType *sb = [[SBEmployeeType alloc]initWithfile];
   //Check status,Is waiting for check out?
    
    _placeNameInput.text = [_placeNameInput.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _customerNameInput.text = [_customerNameInput.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _trackingNoInput.text = [_trackingNoInput.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    

    
    if((_placeNameInput.text.length<=0||_customerInformationLabel.text.length<=0)||([sb isTrackingNoRequireField:[_staffData objectForKey:@"employee_type"]]&&(_trackingNoInput.text.length<=0))||[self.locationLabel.text isEqualToString:@"-"]){
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                    message: @"กรุณากรอกข้อมูลให้ครบ"
                                                   delegate: nil
                                          cancelButtonTitle: @"OK"
                                          otherButtonTitles: nil
                          ];
    [alert show];
    }else{
        _checkOutPoint = [[SBLocation alloc]init];
       [_checkOutPoint directCheckOut];
        _operationFile = [[SBFileOperator alloc]initWithName:@"history" :@".txt" :YES];
        
        if(_current_key_id==[NSNumber numberWithInt:0]){
            _current_key_id = [NSNumber numberWithInt:1];
        //    NSLog(@"%@",_current_key_id);
            [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:1]forKey:@"current_key_id"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }else{
            
          //  NSLog(@"%@",_current_key_id);
            _current_key_id = [NSNumber numberWithInt:(int)[_current_key_id integerValue]+1];
            [[NSUserDefaults standardUserDefaults] setObject:[_current_key_id stringValue] forKey:@"current_key_id"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }

        
            if(_nowLocation!=nil){
                
                NSDate *now = [NSDate date];

                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.dateFormat = @"Y-MM-dd HH:mm:ss";
                [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
                NSLog(@"The Current Time is %@",[dateFormatter stringFromDate:now]);
            
            
              //  NSLog(@"%@",[_operationFile getPathFile]);
                NSString *place_name = [_startpointDic objectForKey:@"place_name"];
                NSString *lat =[_startpointDic objectForKey:@"lat"];
                NSString *lng = [_startpointDic objectForKey:@"lng"];
                if(_hasCheckIntoday){
                    NSDictionary *lastPlaceName = [_operationFile getLastStartPlaceInDay];
                    place_name = [lastPlaceName objectForKey:@"place_name"];
                    lat =[lastPlaceName objectForKey:@"lat"];
                    lng = [lastPlaceName objectForKey:@"lng"];
                }
                CLLocation *startLocation = [[CLLocation alloc]initWithLatitude
                                             :[lat doubleValue] longitude:[lng doubleValue]];
                int distance = [_checkOutPoint Callocation:startLocation point2:_nowLocation];
                SBLocation *point1 = [[SBLocation alloc]initWithDetail:[_current_key_id stringValue]
                                                                  :@"null"
                                                                  :[_staffData objectForKey:@"staff_id"]
                                                                  :_placeNameInput.text
                                                                  :_customerNameInput.text
                                                                  :_trackingNoInput.text
                                                                  :place_name
                                                                  :lat
                                                                  :lng
                                                                  :[NSString stringWithFormat:@"%f",_nowLocation.coordinate.latitude]
                                                                  :[NSString stringWithFormat:@"%f",_nowLocation.coordinate.longitude]
                                                                  :[dateFormatter stringFromDate:now]
                                                                  :@"check_out_time"
                                                                  :[NSString stringWithFormat:@"%@",_locationLabel.text]
                                                                  :@"n/a"
                                                                  :@"acitve"
                                                                  :[_staffData objectForKey:@"employee_type"]
                                                                :[NSString stringWithFormat:@"%d",distance ]];
                [_operationFile.arrayList addObject:point1.dict];
                [_operationFile arrayParseToJson:_operationFile.arrayList];
                
                [_checkOutPoint checkOut];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"สถานะ"
                                                                    message: @"เช็คอินเรียบร้อยแล้ว"
                                                                   delegate: nil
                                                          cancelButtonTitle: @"OK"
                                                          otherButtonTitles: nil
                    ];
                [alert show];
                [self setBlankInput];
                [self viewDidLoad];
                [self viewWillAppear:YES];
            }
        
    }
}

#pragma mark - getNowLocation
- (void)getNowLocation{
    NSLog(@"== Get location on SBLocation ===");
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = 1;
    [self.locationManager setDelegate:self];
   // NSLog(@"Start Update Location on CheckInviewController");
//    [self.locationManager requestWhenInUseAuthorization];
//    [self.locationManager requestAlwaysAuthorization];
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
  //  NSLog(@"locationManager");
    
    if (UIApplication.sharedApplication.applicationState == UIApplicationStateActive){
        NSLog(@"App is foreground. New location is %@", newLocation);
    }else{
        NSLog(@"App is backgrounded. New location is %@", newLocation);
    }
    _nowLocation = newLocation;
    _locationLabel.text = [NSString stringWithFormat:@"%f,%f",newLocation.coordinate.latitude,newLocation.coordinate.longitude ];
    
    self.gobj.Gdelegate = self;
    [self.gobj getLocationFromCLlocation:newLocation];
    //NSLog(@"Stop Update Locaiton CheckInviewController");
    [self.locationManager stopUpdatingLocation];
}
-(void)setLocationAddress:(NSDictionary*)locationAddress{
    _locationLabel.text = [NSString stringWithFormat:@"%@,%@,%@",[locationAddress objectForKey:@"tumbol"],[locationAddress objectForKey:@"aumpur"],[locationAddress objectForKey:@"province"]];
}

-(IBAction)setBlank:(id)sender{
    [self setBlankInput];
}
-(void)setBlankInput{
    _placeNameInput.text = @"";
    _customerNameInput.text = @"";
    _trackingNoInput.text = @"";

}

#pragma mark - Segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:History]) {
        HistoryViewController *destVC = segue.destinationViewController;
        destVC.operationFile = (SBFileOperator *)_operationFile;
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    BOOL check = YES;
    if (check) {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)stopLocationWhenDel{
    NSLog(@"stopLocationWhenDel");
}

#pragma mark - StopNotificationCenter
- (void)stopLocationUrgent:(NSNotification *)notification{
    NSLog(@"stopLocationUrgent");
    [_checkOutPoint stopUpdateLocation];
}

- (void)setLocationFromHistory:(NSNotification*)notification{
   // NSLog(@"hello");
    _placeNameInput.text = [notification.userInfo objectForKey:@"place_name"];
    _customerNameInput.text = [notification.userInfo objectForKey:@"customer_name"];
    _trackingNoInput.text = [notification.userInfo objectForKey:@"tracking_no"];
}


-(void)backToLoginOnCheckIn:(NSNotification*)notification{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PreloaderViewController"];
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:vc animated:NO completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
