//
//  SBStartPointLocation.m
//  SBPoint
//
//  Created by FireOneOne on 5/23/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "SBStartPointLocation.h"
#import "SBFileOperator.h"
@implementation SBStartPointLocation

-(id)initWithPlace:(NSString*)place_name
                  :(NSString*)lat
                  :(NSString *)lng
                  :(NSString*)place_id
                  :(NSString*)status{
    
    _dict = [[NSDictionary alloc]initWithObjectsAndKeys:place_name,@"place_name",lat,@"lat",lng,@"lng",place_id,@"place_id",status,@"status", nil];
    return self;
}
-(void)firstLaunch{
    SBFileOperator *operationFile = [[SBFileOperator alloc] initWithName:@"startLocation" :@".txt" :YES];
    [operationFile.arrayList addObject:self.dict];
    [operationFile arrayParseToJson:operationFile.arrayList];
}
-(void)savePlace{
     SBFileOperator *allStaff = [[SBFileOperator alloc] initWithName:@"startLocation" :@".txt" :YES];
    for(int i=0;i<[allStaff.arrayList count];i++){
        if([[allStaff.arrayList[i] objectForKey:@"status"] isEqualToString:@"active"]){
            NSDictionary *tmpDict = [[NSDictionary alloc]initWithObjectsAndKeys:[allStaff.arrayList[i] objectForKey:@"place_name"],@"place_name",[allStaff.arrayList[i] objectForKey:@"lat"],@"lat",[allStaff.arrayList[i] objectForKey:@"lng"],@"lng",[allStaff.arrayList[i] objectForKey:@"place_id"],@"place_id",@"disable",@"status", nil];
            [allStaff.arrayList addObject:tmpDict];
            [allStaff.arrayList removeObjectAtIndex:i];
        }
    }
    self.dict = [[NSDictionary alloc]initWithObjectsAndKeys:[_dict objectForKey:@"place_name"],@"place_name",[_dict objectForKey:@"lat"],@"lat",[_dict objectForKey:@"lng"],@"lng",[self getPlaceId],@"place_id",[_dict objectForKey:@"status"],@"status", nil];
    
    [allStaff.arrayList addObject:self.dict];
    [allStaff arrayParseToJson:allStaff.arrayList];
    
    
}
-(NSString*)getPlaceId{
    SBFileOperator *operationFile = [[SBFileOperator alloc] initWithName:@"startLocation" :@".txt" :YES];
    int tmp = 0;
    if((int)[operationFile.arrayList count]>0){
        for(int c=0;c<[operationFile.arrayList count];c++){
            if([[operationFile.arrayList[c] objectForKey:@"place_id"] integerValue]>0){
                tmp = (int)[[operationFile.arrayList[c] objectForKey:@"place_id"] integerValue];
            }
        }
    }
    return [NSString stringWithFormat:@"%d",tmp+1 ];
}
-(NSDictionary*)getPlaceActive{
    SBFileOperator *operationFile = [[SBFileOperator alloc] initWithName:@"startLocation" :@".txt" :YES];
    NSDictionary *startPlace = [[NSDictionary alloc]init];
    for(int j=0;j<operationFile.arrayList.count;j++){
        if([[operationFile.arrayList[j] objectForKey:@"status"]isEqualToString:@"active"]){
            startPlace = operationFile.arrayList[j];
        }
    }
    return startPlace;
}
-(void)setStartPoint:(NSString*)place_id{
    NSLog(@"\n Set new Start Point index = %@",place_id);
    SBFileOperator *operationFile = [[SBFileOperator alloc] initWithName:@"startLocation" :@".txt" :YES];
    NSLog(@"Count all start place : %d",(int)[operationFile.arrayList count]);
    NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
    for(int j=0;j<(int)[operationFile.arrayList count];j++){
        NSLog(@"j %d",j);
        NSLog(@"place_id = %@",place_id);
        if([[operationFile.arrayList[j] objectForKey:@"place_id"] isEqualToString:place_id]){
            NSString *place_id_tmp =[operationFile.arrayList[j] objectForKey:@"place_id"];
            NSDictionary *tmp = [[NSDictionary alloc]initWithObjectsAndKeys
                                 :[operationFile.arrayList[j] objectForKey:@"lat"],@"lat"
                                 ,[operationFile.arrayList[j] objectForKey:@"lng"],@"lng"
                                 ,place_id_tmp,@"place_id"
                                 ,[operationFile.arrayList[j] objectForKey:@"place_name"],@"place_name",
                                 @"active",@"status",nil ];
            NSLog(@"set Active %@ %@",[operationFile.arrayList[j] objectForKey:@"place_name"],operationFile.arrayList[j]);
            [tmpArray addObject:tmp];
        }else{
            NSString *place_id_tmp =[operationFile.arrayList[j] objectForKey:@"place_id"];
            NSDictionary *tmp = [[NSDictionary alloc]initWithObjectsAndKeys
                                 :[operationFile.arrayList[j] objectForKey:@"lat"],@"lat"
                                 ,[operationFile.arrayList[j] objectForKey:@"lng"],@"lng"
                                 ,place_id_tmp,@"place_id"
                                 ,[operationFile.arrayList[j] objectForKey:@"place_name"],@"place_name",
                                 @"disable",@"status",nil ];
            NSLog(@"set  disable %@ %@"
                  ,[operationFile.arrayList[j] objectForKey:@"place_name"],operationFile.arrayList[j]);
            [tmpArray addObject:tmp];
        }
        
    }
    for(int j=0;j<[tmpArray count];j++){
        [operationFile removeFromPlaceId:[tmpArray[j] objectForKey:@"place_id"]];
    }
    //add to file
    for(int j=0;j<[tmpArray count];j++){
        [operationFile.arrayList addObject:tmpArray[j]];
    }
    [operationFile arrayParseToJson:operationFile.arrayList];

}
-(void)deletePlace:(int)row{
    SBFileOperator *operationFile = [[SBFileOperator alloc] initWithName:@"startLocation" :@".txt" :YES];
    NSLog(@"oper: %@",[operationFile.arrayList[row] objectForKey:@"place_name"]);
    [operationFile removeFromPlaceId:[operationFile.arrayList[row] objectForKey:@"place_id"]];
    [operationFile arrayParseToJson:operationFile.arrayList];
    
}
@end
