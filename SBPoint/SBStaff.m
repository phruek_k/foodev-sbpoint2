//
//  SBStaff.m
//  SBPoint
//
//  Created by FireOneOne on 5/22/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "SBStaff.h"
#import "SBFileOperator.h"
#import "SBNetwork.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "SBLocation.h"
#define IP @"http://122.155.201.44/sbpointapi"

#import "AFNetworking.h"
@implementation SBStaff
@synthesize SBStaffDelegate;

-(id)initWithStaff:(NSString*)staff_id
                  :(NSString*)firstname
                  :(NSString*)lastname
                  :(NSString*)phone
                  :(NSString*)email
                  :(NSString*)employee_type
                  :(NSString*)key{
    
    _dict = [[NSDictionary alloc]initWithObjectsAndKeys:staff_id,@"staff_id",firstname,@"firstname",lastname,@"lastname",phone,@"phone",email,@"email",employee_type,@"employee_type",key,@"key",nil];
    
    return self;
}
-(NSDictionary*)getStaff{
    SBFileOperator *staffFile = [[SBFileOperator alloc]initWithName:@"staff":@".txt" :YES];
    NSArray *staffData = [staffFile readFileRetunArray];
    return staffData[0];
}
-(void)testregisterStaff{
    SBFileOperator *staffFile = [[SBFileOperator alloc]initWithName:@"staff":@".txt" :YES];
    SBStaff *staffData = [[SBStaff alloc]initWithStaff:@"SB016" :@"testname" :@"testlastname" :@"phone" :@"email" :@"project_sale" :@"null"];
    [staffFile.arrayList addObject:staffData.dict];
    [staffFile arrayParseToJson:staffFile.arrayList];
    SBNetwork *syncNetwork = [[SBNetwork alloc]init];
    [syncNetwork registerStaff:[self getStaff]];
}

-(void)registerStaff{
    NSString *method = @"staff/register";
    NSDictionary *data = self.dict;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@/%@",IP,method];
    NSLog(@"%@",url);
    [manager POST:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"status: upload success");
        
        NSError *e;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
        NSLog(@"status = %@",[jsonDict objectForKey:@"status"]);
        if([[jsonDict objectForKey:@"status"] isEqualToString:@"OK"]){
            [SBStaffDelegate registerNoticComplete:jsonDict];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                            message: @"รหัสพนักงานนี้มีผู้ใช้แล้ว"
                                                           delegate: nil
                                                  cancelButtonTitle: @"ตกลง"
                                                  otherButtonTitles: nil
                                  ];
            [alert show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"status: upload failed");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                        message: @"กรุณาเชื่อมต่ออินเทอร์เน๊ต"
                                                       delegate: nil
                                              cancelButtonTitle: @"ตกลง"
                                              otherButtonTitles: nil
                              ];
        [alert show];
        
    }];
}
-(void)login{
    SBNetwork *login = [[SBNetwork alloc]init];
    login.SBNetworkDelegate = self;
    [login login:self.dict];
}
-(void)loginComplete:(NSDictionary*)jsonDict{
    LoginViewController *loginData = [[LoginViewController alloc]init];
    SBFileOperator *staffFile = [[SBFileOperator alloc]initWithName:@"staff":@".txt" :YES];
    NSString *staff_id = [[jsonDict objectForKey:@"result"] objectForKey:@"staff_id"];
    NSString *firstname = [[jsonDict objectForKey:@"result"] objectForKey:@"firstname"];
    NSString *lastname = [[jsonDict objectForKey:@"result"] objectForKey:@"lastname"];
    NSString *phone = [[jsonDict objectForKey:@"result"] objectForKey:@"phone"];
    NSString *email = [[jsonDict objectForKey:@"result"] objectForKey:@"email"];
    NSString *employee_type = [[jsonDict objectForKey:@"result"] objectForKey:@"employee_type"];
    NSString *key = [[jsonDict objectForKey:@"result"] objectForKey:@"key"];
    SBStaff *staffData = [[SBStaff alloc]initWithStaff:staff_id :firstname :lastname :phone :email :employee_type :key];
    [staffFile.arrayList addObject:staffData.dict];
    [staffFile arrayParseToJson:staffFile.arrayList];
    
    [loginData loginComplete];
    NSLog(@"login Complete");
}
-(void)loginFail{
    NSLog(@"login fail");
}
-(void)registerStaffComplete:(NSDictionary*)jsonDict{
    [SBStaffDelegate registerNoticComplete:jsonDict];
}
-(void)registerStaffFail:(NSDictionary*)jsonDict{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน" message: @"มีรหัสสมาชิกนี้ใช้งานอยู่แล้ว"
                                                                        delegate: nil
                                                                cancelButtonTitle: @"ตกลง"
                                                                otherButtonTitles: nil];
    [alert show];
}
-(void)changeEmployeeType:(NSString*)employee_type{
    SBFileOperator *staffFile = [[SBFileOperator alloc]initWithName:@"staff":@".txt" :YES];
    _dict = [[NSDictionary alloc]initWithObjectsAndKeys:[staffFile.arrayList[0] objectForKey:@"staff_id"],@"staff_id",[staffFile.arrayList[0] objectForKey:@"firstname"],@"firstname",[staffFile.arrayList[0] objectForKey:@"lastname"],@"lastname",[staffFile.arrayList[0] objectForKey:@"phone"],@"phone",[staffFile.arrayList[0] objectForKey:@"email"],@"email",employee_type,@"employee_type",[staffFile.arrayList[0] objectForKey:@"key"],@"key",nil];
    [staffFile deleteStaffFile];
    [staffFile.arrayList addObject:_dict];
    [staffFile arrayParseToJson:staffFile.arrayList];

}
-(NSString*)getStaffEmployeeType{
    SBFileOperator *staffFile = [[SBFileOperator alloc]initWithName:@"staff":@".txt" :YES];
    return [staffFile.arrayList[0] objectForKey:@"employee_type"];
}

-(NSString*)getStaffId{
    SBFileOperator *staffFile = [[SBFileOperator alloc]initWithName:@"staff":@".txt" :YES];
    return [staffFile.arrayList[0] objectForKey:@"staff_id"];
}

-(void)editStaff:(NSDictionary*)staffData{
    
    SBNetwork *staffSync = [[SBNetwork alloc]init];
    [staffSync startWaiting];
    
    NSString *method = @"staff/edit";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@/%@",IP,method];
    NSLog(@"%@",url);
    [manager POST:url parameters:staffData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"status: upload success");
        
        NSError *e;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
        NSLog(@"check staff result = %@",[jsonDict objectForKey:@"result"]);
        if([[jsonDict objectForKey:@"status"]isEqualToString:@"OK"]){
            SBFileOperator *staffFile = [[SBFileOperator alloc]initWithName:@"staff":@".txt" :YES];
            [staffFile.arrayList removeAllObjects];
            [staffFile.arrayList addObject:staffData];
            [staffFile arrayParseToJson:staffFile.arrayList];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"ข้อความ"
                                                            message: @"อัพเดทข้อมูลเรียบร้อย"
                                                           delegate: nil
                                                  cancelButtonTitle: @"OK"
                                                  otherButtonTitles: nil
                                  ];
            [alert show];

            [[NSNotificationCenter defaultCenter] postNotificationName:@"backToCheckIn" object:self userInfo:nil];
            [staffSync stopWaiting];

        }else if([[jsonDict objectForKey:@"status"]isEqualToString:@"No data update"]){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"backToCheckIn" object:self userInfo:nil];
            [staffSync stopWaiting];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                            message: @"กรุณาลองใหม่อีกครั้ง"
                                                           delegate: nil
                                                  cancelButtonTitle: @"OK"
                                                  otherButtonTitles: nil
                                  ];
            [alert show];
            [staffSync stopWaiting];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"status: upload failed");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                        message: @"กรุณาเชื่อมต่ออินเทอร์เน็ต"
                                                       delegate: nil
                                              cancelButtonTitle: @"OK"
                                              otherButtonTitles: nil
                              ];
        [alert show];
        [staffSync stopWaiting];
    }];


}
-(void)clearStaffInformation{
    SBFileOperator *staffFile = [[SBFileOperator alloc]initWithName:@"staff":@".txt" :YES];
    [staffFile.arrayList removeAllObjects];
    [staffFile arrayParseToJson:staffFile.arrayList];
}
-(void)checkBeforeEditStaff:(NSDictionary*)staffData{
    SBNetwork *staffSync = [[SBNetwork alloc]init];
    [staffSync startWaiting];
    NSDictionary *data = [[NSDictionary alloc]initWithObjectsAndKeys:[staffData objectForKey:@"staff_id"],@"staff_id", nil];
    SBStaff *staff = [[SBStaff alloc]init];
    NSDictionary *staffDict = [staff getStaff];
    
     NSString *method = @"staff/getStaffInfo";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@/%@",IP,method];
    NSLog(@"%@",url);
    [manager POST:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"status: upload success");
        [staffSync stopWaiting];
        NSError *e;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
        NSLog(@"check staff result = %@",[jsonDict objectForKey:@"result"]);
        if([[jsonDict objectForKey:@"status"]isEqualToString:@"OK"]){
            
            NSString *serverKey = [NSString stringWithFormat:@"%@",[[jsonDict objectForKey:@"result"] objectForKey:@"key"]];
            if(!([serverKey isEqualToString:[staffDict objectForKey:@"key"]])||serverKey.length==0){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"backToLogin" object:self userInfo:nil];
                [self clearStaffInformation];
                SBLocation *location = [[SBLocation alloc]init];
                [location clearAllLocation];
            }else{
                [self editStaff:staffData];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [staffSync stopWaiting];
        NSLog(@"status: upload failed");
    }];

}
-(BOOL)hasStaffFile{
    SBFileOperator *staffFile = [[SBFileOperator alloc]initWithName:@"staff":@".txt" :YES];
    if([staffFile.arrayList count]>0){
        return YES;
    }else{
        return NO;
    }
}

@end
