//
//  LoginViewController.h
//  SBPoint
//
//  Created by FireOneOne on 5/23/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBStaff.h"
@interface LoginViewController : UIViewController<UITextFieldDelegate>
-(void)loginComplete;
@end
