//
//  StartPlaceCell.m
//  SBPoint
//
//  Created by FireOneOne on 5/23/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "StartPlaceCell.h"

@implementation StartPlaceCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    
    // Initialization code
    _placeName.font = [UIFont fontWithName:@"DBHelvethaicaX-Med" size:18];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
