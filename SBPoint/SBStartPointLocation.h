//
//  SBStartPointLocation.h
//  SBPoint
//
//  Created by FireOneOne on 5/23/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBStartPointLocation : NSObject
@property(strong,nonatomic)NSString *place_name;
@property(strong,nonatomic)NSString *lat;
@property(strong,nonatomic)NSString *lng;
@property(strong,nonatomic)NSString *place_id;
@property(strong,nonatomic)NSString *status;
@property(strong,nonatomic)NSDictionary* dict;
-(id)initWithPlace:(NSString*)place_name
                  :(NSString*)lat
                  :(NSString *)lng
                  :(NSString*)place_id
                  :(NSString*)status;
-(void)savePlace;
-(void)deletePlace;
-(void)setStartPoint:(NSString*)place_id;
-(NSDictionary*)getPlaceActive;
-(void)firstLaunch;
-(void)deletePlace:(int)row;
@end
