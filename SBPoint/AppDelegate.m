//
//  AppDelegate.m
//  SBPoint
//
//  Created by FireOneOne on 5/17/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "AppDelegate.h"
#import "SBStaff.h"
#import "SBLocation.h"
#import "SBStartPointLocation.h"
#import "SBTrashLocation.h"
#import "SBEmployeeType.h"

#import "AutoCheckVersion.h"
@interface AppDelegate()
@property AutoCheckVersion *file;
@end
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"Lunch on IOS Version : %@",[[UIDevice currentDevice] systemVersion]);
    
    SBEmployeeType *sb = [[SBEmployeeType alloc]initWithfile];
    [sb setEmployeeToDoc];
    
   NSDictionary * navBarTitleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor],
     NSFontAttributeName:[UIFont fontWithName:@"DBHelvethaicaX-Med" size:23]
    };
    
    [[UINavigationBar appearance] setTitleTextAttributes:navBarTitleTextAttributes];
    
    double version = [[[UIDevice currentDevice] systemVersion] doubleValue];
    if(version>6.1){
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"]){
        SBLocation *location = [[SBLocation alloc]init];
        [location directCheckOut];
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"This is the first launch ever");
        SBStartPointLocation *firstPlaceObj = [[SBStartPointLocation alloc]initWithPlace :@"ปากเกร็ด" :@"13.911667" :@"100.548182" :@"1" :@"active"];
        [firstPlaceObj firstLaunch];
    }
    
    SBStaff *staffObj = [[SBStaff alloc]init];
    if([staffObj hasStaffFile]){
        SBTrashLocation *syncTrashFile = [[SBTrashLocation alloc]init];
        [syncTrashFile syncToDeleteToServer];
    }
    
    NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:4 * 1024 * 1024
                                                         diskCapacity:20 * 1024 * 1024
                                                             diskPath:nil];
    [NSURLCache setSharedURLCache:URLCache];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    SBEmployeeType *sb = [[SBEmployeeType alloc]initWithfile];
    [sb setEmployeeToDoc];
    
    self.file = [[AutoCheckVersion alloc]init];
    [self.file initWithVerify];
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    SBLocation *location = [[SBLocation alloc]init];
    [location directCheckOut];
    SBLocation *syncFile = [[SBLocation alloc]init];
    [syncFile checkLocationToSync];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
