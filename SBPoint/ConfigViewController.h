//
//  ConfigViewController.h
//  SBPoint
//
//  Created by FireOneOne on 5/20/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const MHCustomTabBarControllerViewControllerChangedNotification;
extern NSString *const MHCustomTabBarControllerViewControllerAlreadyVisibleNotification;

@interface ConfigViewController : UIViewController

@property (weak,nonatomic) UIViewController *destinationViewController;
@property (strong, nonatomic) NSString *destinationIdentifier;
@property (strong, nonatomic) UIViewController *oldViewController;
@property (weak, nonatomic) IBOutlet UIView *container;
@property (nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@end
