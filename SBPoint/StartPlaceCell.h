//
//  StartPlaceCell.h
//  SBPoint
//
//  Created by FireOneOne on 5/23/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol StartPlaceCellDelegate <NSObject>
- (void) deleteStartPlace:(int)row;
@end
@interface StartPlaceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *deleteStartplaceButton;
@property (weak, nonatomic) IBOutlet UILabel *placeName;
@property (weak, nonatomic) id <StartPlaceCellDelegate> delegate;
@end

