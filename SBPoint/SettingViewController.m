//
//  SettingViewController.m
//  SBPoint
//
//  Created by FireOneOne on 5/18/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "SettingViewController.h"
#import "StartPointViewController.h"
#import "SBStartPointLocation.h"
#import "SBStaff.h"
#import "SBTrashLocation.h"
@interface SettingViewController ()<StartPlaceCellDelegate,StartPointDelegate,UIAlertViewDelegate>{
    NSMutableArray *myObject;
    NSDictionary *dict;
    NSArray *arrStatus;
    
}

@property (weak, nonatomic) IBOutlet UITextField *placeNameLabel;
@property (weak, nonatomic) IBOutlet UITableView *startPlaceTabel;
@property (weak, nonatomic) IBOutlet UILabel *topicStartPointLabel;
@property (nonatomic) BOOL isFirstLuanch;
@property (weak, nonatomic) IBOutlet UIView *settingStartPointView;
@property (strong, nonatomic)SBStaff *staffObj;
@property int tmpIndexRow;
@end
@implementation SettingViewController
#define isiPhone5  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE
-(void)deleteStartPlace:(int)row{

}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    _place_name = @"-";
    [self initStyle];
    _staffObj = [[SBStaff alloc]init];
}

- (void)initStyle{
    _placeNameLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    _topicStartPointLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Med" size:20];
    UIColor *color = [UIColor whiteColor];
    _placeNameLabel.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"ชื่อสถานที่" attributes:@{NSForegroundColorAttributeName: color}];
}

-(void) viewDidAppear:(BOOL)animated{
    self.isFirstLuanch = NO;
    _startPointFile = [[SBFileOperator alloc] initWithName:@"startLocation" :@".txt" :YES];
    [_startPlaceTabel reloadData];
    for(int i=0;i<[_startPointFile.arrayList count];i++){
        if([[_startPointFile.arrayList[i] objectForKey:@"status"] isEqualToString:@"active"]){
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [self.startPlaceTabel selectRowAtIndexPath:indexPath
                                              animated:YES
                                        scrollPosition:UITableViewScrollPositionNone];
            [self tableView:self.startPlaceTabel didSelectRowAtIndexPath:indexPath];
        }
    }

    
}

- (void)viewWillAppear:(BOOL)animated{
    if (isiPhone5){
        // this is iphone 4 inch
        
    }else{
        //Iphone  3.5 inch
//        //        _settingStartPointView.frame = CGRectMake(_settingStartPointView.frame.origin.x,_settingStartPointView.frame.origin.y-50,_settingStartPointView.frame.size.width,_settingStartPointView.frame.size.height);
//       // [_settingStartPointView removeFromSuperview];
//        [_settingStartPointView setTranslatesAutoresizingMaskIntoConstraints:YES];
//        NSLog(@"%d",(int)_settingStartPointView.frame.origin.y);
//        [_settingStartPointView setFrame:CGRectMake(_settingStartPointView.frame.origin.x,100,_settingStartPointView.frame.size.width,_settingStartPointView.frame.size.height)];
//        
//        [_picker setTranslatesAutoresizingMaskIntoConstraints:YES];
//        [_picker setFrame:CGRectMake(_picker.frame.origin.x,_picker.frame.origin.y,_picker.frame.size.width,50)];
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;

}

- (IBAction)delAction:(id)sender{
    UIButton *button = (UIButton *)sender;
    int indexRow = (int)button.tag-100;
    
    if([_startPointFile.arrayList count]<=1){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                        message: @"ต้องมีอย่างน้อยหนึ่งจุดเริ่มต้น"
                                                       delegate: nil
                                              cancelButtonTitle: @"ตกลง"
                                              otherButtonTitles: nil
                              ];
        [alert show];
    }else{
        if([[_startPointFile.arrayList[indexRow] objectForKey:@"status"] isEqualToString:@"active"]){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                            message: @"จุดเริ่มต้นนี้ถูกใช้งานอยู่"
                                                           delegate: nil
                                                  cancelButtonTitle: @"ตกลง"
                                                  otherButtonTitles: nil,nil];
            [alert show];
            self.tmpIndexRow = indexRow;

        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                            message: @"ต้องการลบจุดเริ่มต้น"
                                                           delegate: self
                                                  cancelButtonTitle: @"ยกเลิก"
                                                  otherButtonTitles: @"ตกลง",nil];
            [alert show];
            self.tmpIndexRow = indexRow;
        }
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    NSLog(@"=== Delegate to delete startpoint ===");
    if (buttonIndex == 0) {
        NSLog(@"Status : cancel");
    }else{
        NSLog(@"Status : delete");
        SBStartPointLocation *delPlace = [[SBStartPointLocation alloc]init];
        [delPlace deletePlace:self.tmpIndexRow];
        self.tmpIndexRow = 0;
        [self viewDidAppear:YES];
    }
    
}
#pragma mark - segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"startPoint"]){
        StartPointViewController *sp = [segue destinationViewController];
        [sp setPlaceName:_placeNameLabel.text];
        sp.myDelegate = self;
        _placeNameLabel.text = @"";
        
    }
    
}
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if([identifier isEqualToString:@"startPoint"]){
        if([_placeNameLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0){
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"คำเตือน" message:@"กรุณากรอกชื่อสถานที่" delegate:nil cancelButtonTitle:@"ตกลง" otherButtonTitles:nil, nil];
            [alertView show];
             return NO;
        }else{
            return YES;
        }
    }else{
        return YES;
    }
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _startPointFile.arrayList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    StartPlaceCell *startPlaceCell = [tableView dequeueReusableCellWithIdentifier:@"StartPlaceCell"];
    startPlaceCell.delegate = self;
    startPlaceCell.placeName.text = [_startPointFile.arrayList[indexPath.row] objectForKey:@"place_name"];
    if([[_startPointFile.arrayList[indexPath.row] objectForKey:@"status"]isEqualToString:@"active"]){
        startPlaceCell.deleteStartplaceButton.hidden = YES;
    }else{
        startPlaceCell.deleteStartplaceButton.hidden = NO;
    }
    startPlaceCell.imageView.image = [UIImage imageNamed:@"radioBlankButton.png"];
    startPlaceCell.imageView.highlightedImage = [UIImage imageNamed:@"radioCheckButton.png"];
    
    startPlaceCell.deleteStartplaceButton.tag = indexPath.row+100;
    
    
    UIImageView *bgView = [[UIImageView alloc]initWithFrame:startPlaceCell.frame];
    bgView.backgroundColor = [UIColor clearColor];
    startPlaceCell.selectedBackgroundView = bgView;
    
    
    return startPlaceCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.isFirstLuanch){
        SBStartPointLocation *setStartPoint = [[SBStartPointLocation alloc]init];
        [setStartPoint setStartPoint:[_startPointFile.arrayList[indexPath.row] objectForKey:@"place_id"]];
        [self viewDidAppear:YES];
    }
    self.isFirstLuanch = YES;
}

#pragma mark - delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    BOOL check = YES;
    if (check) {
        [textField resignFirstResponder];
    }
    return YES;
}
- (void)refresh{
    [self viewDidAppear:YES];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
