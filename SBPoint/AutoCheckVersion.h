//
//  AutoCheckVersion.h
//  autoUpdate
//
//  Created by  Phruek Kosonsuwiwat on 7/28/14.
//  Copyright (c) 2014 Fireoneone co.,ltd . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface AutoCheckVersion : NSObject <UIAlertViewDelegate>{
    
}

-(void)initWithVerify;
-(NSString*)getDataFromUserDefault:(NSString*)key;
-(void)setDataToUserDefaultWithBool:(BOOL)value key:(NSString*)key;
-(void)setDataToUserDefault:(NSString*)value key:(NSString*)key;
-(NSString*)getIdentityVersion;
-(NSString*)getIdentityBuild;
-(NSString*)getIdentityBundleName;

-(void)alertViewToUpdate;

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;
-(void)getVersion:(NSDictionary*)param
  successCallBack:(void (^)(NSDictionary*)) successCallback
    errorCallback:(void (^)(NSError *error,NSString *errorMsg))errorCallback;
- (void) makeRequestTo:(NSString *) urlStr
            parameters:(NSDictionary *) params
       successCallback:(void (^)(id jsonResponse)) successCallback
         errorCallback:(void (^)(NSError * error, NSString *errorMsg)) errorCallback;
@end
