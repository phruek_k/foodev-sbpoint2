//
//  SBEmployeeType.m
//  com.voize.SBPoint
//
//  Created by Siang on 7/8/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "SBEmployeeType.h"


@implementation SBEmployeeType
- (id) initWithfile{
    self.file = [[SBFileOperator alloc]initWithName:@"employee_type":@".txt" :YES];
    self.arr = [self.file readFileRetunArray];
    return self;
}
- (void) setEmployeeToDoc{
    NSLog(@"=== Get Employee Type from API ===");
    NSString *url = @"http://122.155.201.44/sbpointapi/staff/getEmployeeType";
    [self getEmployeeFormPost:url parameters:nil successCallback:^(id jsonResponse) {
        NSLog(@"Status : Success ");
        NSDictionary *jsonDict = jsonResponse;
        NSArray *employee_type_array = [jsonDict objectForKey:@"result"];
       // NSDictionary *zero = [employee_type_array objectAtIndex:0];
        [self.file arrayParseToJson:employee_type_array];
    } errorCallback:^(NSError *error, NSString *errorMsg) {
        NSLog(@"Status : Fail ");
        NSLog(@"%@",errorMsg);
    }];
}
- (void) getEmployeeFormPost:(NSString *) urlStr
                parameters:(NSDictionary *) params
           successCallback:(void (^)(id jsonResponse)) successCallback
             errorCallback:(void (^)(NSError * error, NSString *errorMsg)) errorCallback{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:urlStr parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"status: upload success");
        NSError *e;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
        successCallback(jsonDict);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorCallback(error, nil);
    }];
    
}
- (NSArray *)getArrayEmployeeType{
    NSMutableArray *arrEmployeeType = [[NSMutableArray alloc]init];
    for (int i=0;i<[self.arr count]; i++) {
        NSString *employee_type =[[[self.arr objectAtIndex:i] objectForKey:@"employee_type"] uppercaseString];
        [arrEmployeeType addObject:[employee_type stringByReplacingOccurrencesOfString:@"_" withString:@" "] ];
    }
    return arrEmployeeType;
}
- (BOOL)isTrackingNoRequireField:(NSString *)employee_type{
    BOOL result = NO;
    
    for(int i=0;i<[self.arr count];i++){
        if([[[self.arr objectAtIndex:i]objectForKey:@"employee_type"]isEqualToString:employee_type]){
            if([[[self.arr objectAtIndex:i]objectForKey:@"is_tracking_req"] isEqualToString:@"1"]){
                result = YES;
            }else{
                result = NO;
            }
        }
    }
    return result;
}
@end
