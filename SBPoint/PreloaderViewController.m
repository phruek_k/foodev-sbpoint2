//
//  PreloaderViewController.m
//  SBPoint
//
//  Created by FireOneOne on 5/23/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "PreloaderViewController.h"

@interface PreloaderViewController ()

@end

@implementation PreloaderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
   
}

-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"State : Preloader");
    NSLog(@"Check Staff data");
    
    SBStaff *myStaff = [[SBStaff alloc] init];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if(![myStaff hasStaffFile]){
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:NO completion:nil];
    }else{
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"Navigate"];
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:vc animated:YES completion:nil];
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
