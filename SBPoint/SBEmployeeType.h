//
//  SBEmployeeType.h
//  com.voize.SBPoint
//
//  Created by Siang on 7/8/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "SBFileOperator.h"

@interface SBEmployeeType : NSObject
- (id) initWithfile;
- (void) setEmployeeToDoc;
- (void) getEmployeeFormPost:(NSString *) urlStr
                  parameters:(NSDictionary *) params
             successCallback:(void (^)(id jsonResponse)) successCallback
               errorCallback:(void (^)(NSError * error, NSString *errorMsg)) errorCallback;
- (NSArray *)getArrayEmployeeType;
- (BOOL)isTrackingNoRequireField:(NSString *)employee_type;
@property(strong, nonatomic)SBFileOperator *file;
@property(strong, nonatomic)NSArray *arr;
@end

