//
//  AutoCheckVersion.m
//  autoUpdate
//
//  Created by Siang on 7/28/14.
//  Copyright (c) 2014 Phruek. All rights reserved.
//
#define linkURL @"http://www.fire11.com/AppBucket/check/app"

#import "AutoCheckVersion.h"
#import "AFNetworking.h"
@interface AutoCheckVersion()
@property(strong,nonatomic)NSString* sha1;
@property(strong,nonatomic)NSString* fileName;
@property(strong,nonatomic)NSString* datemodified;
@property(strong,nonatomic)NSString* version;
@property(strong,nonatomic)NSString* url;
@property(strong,nonatomic)NSString* appinfo;
@property(strong,nonatomic)UIAlertView *alertView;
@end
@implementation AutoCheckVersion

-(NSString*)getDataFromUserDefault:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] stringForKey:key];
}

-(void)setDataToUserDefaultWithBool:(BOOL)value key:(NSString*)key{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)setDataToUserDefault:(NSString*)value key:(NSString*)key{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)getIdentityVersion{
   return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

-(NSString*)getIdentityBuild{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
}

-(NSString*)getIdentityBundleName{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
}


-(void)alertViewToUpdate{
    self.alertView = [[UIAlertView alloc] initWithTitle:@"Notice"
                                                    message:@"Your app has old version. Do you want to update?"
                                                   delegate:self
                                          cancelButtonTitle:@"Ignore"
                                          otherButtonTitles:@"Update",nil];
    [self.alertView show];
}
-(void)initWithVerify{
   
    NSString *allBundleName = [self getIdentityBundleName];
    NSArray *bundleNameArray = [allBundleName componentsSeparatedByString:@"."];
    NSString *realName = bundleNameArray[[bundleNameArray count]-1];
    
    self.fileName = [NSString stringWithFormat:@"%@.ipa",realName];
    self.datemodified = [NSString stringWithFormat:@"%@",[self getIdentityBuild]];
    self.version = [self getIdentityVersion];

    if ([self getDataFromUserDefault:@"HasLaunchedOnceAutoUpdate"]){
        NSLog(@"=== Normally Launch ===");
        self.sha1 = [self getDataFromUserDefault:@"sha1"];
        self.appinfo = [self getDataFromUserDefault:@"appinfo"];
        
        if(self.sha1.length<1){
            self.sha1 = @"nosha1";
        }
        
        
        NSDictionary *oldDict = [[NSDictionary alloc]initWithObjectsAndKeys:self.sha1,@"sha1",
                                                                            self.fileName,@"name",
                                                                            self.datemodified,@"datemodified",
                                                                           self.version,@"version"
                                                                            ,nil];
        
        
       // NSLog(@"%@",oldDict);
        

        [self getVersion:oldDict successCallBack:^(NSDictionary *returnParam){
            int status = ([[returnParam objectForKey:@"status"] doubleValue])*10;
            switch (status) {
                case 4000://file not found
                    [self alertToNotice];
                    break;
                case 4040://file not found
                    [self alertToNotice];
                    break;
                case 2000:
                    // You already have the latest version of the file
                    NSLog(@"You have a lastes version.");
                    break;
                case 2001://200.1, "You have different SHA1, Your file is older."
                case 2002://200.2, "You have different SHA1, Your file is newer."
                    self.appinfo = [[returnParam objectForKey:@"data"] objectForKey:@"appinfo"];
                    self.sha1 = [[returnParam objectForKey:@"data"]objectForKey:@"sha1"];
                     NSLog(@"appinfo : %@",self.appinfo);
                    [self alertViewToUpdate];
                    break;
                default:
                    // do nothing
                    break;
            }
           
        } errorCallback:^(NSError *error, NSString *errorMsg){
            NSLog(@"ERROR : %@",errorMsg);
        }];
        
    }else{
        NSLog(@"=== First Launch ===");
        [self setDataToUserDefaultWithBool:YES key:@"HasLaunchedOnceAutoUpdate"];
       
        self.sha1 = @"";
       
        
        NSDictionary *checkFileParameters = @{@"sha1":self.sha1,@"name":self.fileName,@"datemodified":self.datemodified,@"version":self.version};
        [self getVersion:checkFileParameters successCallBack:^(NSDictionary *returnParam){
             NSLog(@"Updating Status : %@",[returnParam objectForKey:@"message"]);
            int statusCode = ([[returnParam objectForKey:@"status"]doubleValue]*10);
            switch (statusCode) {
                case 4000:
                    NSLog(@"******* Your application's name not found!");
                    break;
                case 4040:
                    NSLog(@"******** Your application's name not found!");
                    break;
                case 2000:
                    [self setDataToUserDefault:(NSString*)[[returnParam objectForKey:@"data"]objectForKey:@"sha1"] key:(NSString*)@"sha1"];
                    
                     self.appinfo = [[returnParam objectForKey:@"data"] objectForKey:@"appinfo"];
                    [self setDataToUserDefault:self.appinfo key:@"appinfo"];
                    NSLog(@"Set up sha1 complete!");
                    NSLog(@"====================");
                    break;
                case 2001:
                    self.url = [[returnParam objectForKey:@"data"] objectForKey:@"url"];
                    self.appinfo = [[returnParam objectForKey:@"data"] objectForKey:@"appinfo"];
                    [self setDataToUserDefault:self.appinfo key:@"appinfo"];
                    [self alertViewToUpdate];
                    break;
                default:
                    break;
            }
            
        } errorCallback:^(NSError *error, NSString *errorMsg){
            NSLog(@"ERROR : %@",errorMsg);
            
        }];
        
       
    }
}
-(void)alertToNotice{
    UIAlertView *alertViewNotice = [[UIAlertView alloc] initWithTitle:@"Notice"
                                                message:@"Not found your file on AppBucket. Please contact staff."
                                               delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil,nil];
    [alertViewNotice show];
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    switch (buttonIndex)
    {
        case 0:
            //ignore
            break;
        case 1:
            [self setDataToUserDefault:self.sha1 key:@"sha1"];
             [self setToUpdateNewVersion];
            break;
        default:
            break;
    }
}
-(void)setToUpdateNewVersion{
    NSLog(@"=== Let's Update Version ===");
   /* NSString *urlStr = @"itms-services://?action=download-manifest&url=https://dl.dropboxusercontent.com/s/sxkb3b69qzrlblr/SBPoint2.plist";*/
    NSString *urlStr2 = [NSString stringWithFormat:@"itms-services://?action=download-manifest&url=%@",self.appinfo];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr2]];
}

-(void)getVersion:(NSDictionary*)param
           successCallBack:(void (^)(NSDictionary*)) successCallback
             errorCallback:(void (^)(NSError *error,NSString *errorMsg))errorCallback{
    
    
      [self makeRequestTo:linkURL parameters:param successCallback:^(id jsonResponse) {
        NSLog(@"get Version response%@",jsonResponse);
        NSDictionary *returnDict = jsonResponse;
        successCallback(returnDict);
    }errorCallback:^(NSError *error, NSString *errorMsg){
         NSLog(@"%@",errorMsg);
    }];
}
- (void) makeRequestTo:(NSString *) func
            parameters:(NSDictionary *) params
       successCallback:(void (^)(id jsonResponse)) successCallback
         errorCallback:(void (^)(NSError * error, NSString *errorMsg)) errorCallback{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *urlStr = [NSString stringWithFormat:@"%@",func];
    [manager POST:urlStr parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *e;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
        successCallback(jsonDict);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorCallback(error, nil);
    }];
}
@end

