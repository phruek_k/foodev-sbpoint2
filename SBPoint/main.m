//
//  main.m
//  SBPoint
//
//  Created by FireOneOne on 5/17/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
