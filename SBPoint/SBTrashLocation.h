//
//  SBTrashLocation.h
//  com.voize.SBPoint
//
//  Created by Micheal Carrick on 6/10/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBFileOperator.h"
@interface SBTrashLocation : NSObject
-(void)setDeleteLocationToTrashFile:(NSString*)work_log_id;
-(void)syncToDeleteToServer;
@property(nonatomic,strong)SBFileOperator *operationFile;
@end
