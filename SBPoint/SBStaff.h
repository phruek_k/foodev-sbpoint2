//
//  SBStaff.h
//  SBPoint
//
//  Created by FireOneOne on 5/22/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBNetwork.h"
@protocol SBStaffDelegate <NSObject>
- (void)registerNoticComplete:(NSDictionary*)jsonDict;
- (void)registerNoticFail:(NSDictionary*)jsonDict;
@end

@interface SBStaff : NSObject<SBNetWorkDelegate>
@property (nonatomic , strong)id <SBStaffDelegate>SBStaffDelegate;
@property(strong,nonatomic)NSDictionary* dict;
-(id)initWithStaff:(NSString*)staff_id
                  :(NSString*)firstname
                  :(NSString*)lastname
                  :(NSString*)phone
                  :(NSString*)email
                  :(NSString*)employee_type
                  :(NSString*)key;
-(NSDictionary*)getStaff;
-(void)registerStaff;
-(void)testregisterStaff;
-(void)login;
-(void)registerStaffComplete:(NSDictionary*)jsonDict;
-(void)registerStaffFail:(NSDictionary*)jsonDict;
-(void)changeEmployeeType:(NSString*)employee_type;
-(NSString*)getStaffEmployeeType;
-(void)editStaff:(NSDictionary*)staffData;
-(void)checkBeforeEditStaff:(NSDictionary*)staffData;
-(NSString*)getStaffId;
-(void)clearStaffInformation;
-(BOOL)hasStaffFile;
@end
