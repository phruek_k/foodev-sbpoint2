//
//  SBNetwork.h
//  SBPoint
//
//  Created by FireOneOne on 5/22/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol SBNetWorkDelegate <NSObject>
- (void)loginComplete:(NSDictionary*)jsonDict;
- (void)loginFail;
- (void)registerStaffComplete:(NSDictionary*)jsonDict;
- (void)registerStaffFail:(NSDictionary*)jsonDict;
@end
@interface SBNetwork : NSObject
- (NSDictionary*)addWorkLog:(NSDictionary*)data;
- (BOOL)checkSaff:(NSDictionary*)data;
- (BOOL)checkWorkLog:(NSDictionary*)data;
- (BOOL)delWorkLog:(NSDictionary*)data;
- (BOOL)login:(NSDictionary*)data;
- (BOOL)logout:(NSDictionary*)data;
- (BOOL)registerStaff:(NSDictionary*)data;
- (BOOL)sycnWorkLog:(NSDictionary*)data;
@property (nonatomic , strong)id <SBNetWorkDelegate>SBNetworkDelegate;
- (void)startWaiting;
- (void)stopWaiting;
@end
