//
//  SBTrashLocation.m
//  com.voize.SBPoint
//
//  Created by Micheal Carrick on 6/10/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "SBTrashLocation.h"
#define IP @"http://122.155.201.44/sbpointapi"

#import "AFNetworking.h"
@implementation SBTrashLocation
-(void)setDeleteLocationToTrashFile:(NSString*)work_log_id{
    self.operationFile = [[SBFileOperator alloc]initWithName:@"trash" :@".txt" :YES];
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:work_log_id,@"work_log_id", nil];
    [self.operationFile.arrayList addObject:dict];
    [self.operationFile arrayParseToJson:self.operationFile.arrayList];
}
-(void)syncToDeleteToServer{
    NSLog(@"=== Sync delete file ===");
    self.operationFile = [[SBFileOperator alloc]initWithName:@"trash" :@".txt" :YES];
    NSLog(@"Total : %d delete",(int)[self.operationFile.arrayList count]);
    for(int i=0;i<[self.operationFile.arrayList count];i++){
       NSString *work_log_id = [self.operationFile.arrayList[i] objectForKey:@"work_log_id"];
        
        NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:work_log_id,@"work_log_id", nil];
        NSString *method = @"workLog/delWorkLog";
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *url = [NSString stringWithFormat:@"%@/%@",IP,method];
        //NSLog(@"%@",url);
        [manager POST:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
          //  NSLog(@"status: upload success");
            NSError *e;
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
            NSLog(@"Delete work_log_id : %@ => %@",[data objectForKey:@"work_log_id"],[jsonDict objectForKey:@"result"]);
            if([[jsonDict objectForKey:@"result"] isEqualToString:@"OK"]){
                [self.operationFile removeListByWorkLogId:[data objectForKey:@"work_log_id"]];
                [self.operationFile arrayParseToJson:self.operationFile.arrayList];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"status: upload failed");
            NSLog(@"Status = Not to Connect Internet");
        }];
    }
}
@end
