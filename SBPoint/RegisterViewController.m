//
//  RegisterViewController.m
//  SBPoint
//
//  Created by FireOneOne on 5/23/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "RegisterViewController.h"

#import "SBStaff.h"
#import "SBFileOperator.h"
#import "SBEmployeeType.h"
@interface RegisterViewController ()<SBStaffDelegate>
@property(strong,nonatomic)SBStaff *staff;
@property(strong,nonatomic)NSString *staffType;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UILabel *customerTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *topicRegisterLabel;
@property (weak, nonatomic) IBOutlet UILabel *topicTypeLabel;
@property(strong,nonatomic)NSArray *arrStatus;
@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    SBEmployeeType *sb = [[SBEmployeeType alloc]initWithfile];
    self.arrStatus = [sb getArrayEmployeeType];
    self.staffType = @"project_sale";
    [self.picker setHidden:YES];
    [self initStyle];
   
}

-(void)initStyle{
   
    
    _staffIdInput.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    _firstnameInput.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    _lastnameInput.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    _phoneInput.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    _emailInput.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    
    _topicRegisterLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Med" size:20];
    _customerTypeLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Med" size:20];
    _topicTypeLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Med" size:20];

    UIColor *color = [UIColor whiteColor];
    _staffIdInput.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"รหัสพนักงาน" attributes:@{NSForegroundColorAttributeName: color}];
    _firstnameInput.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"ชื่อ" attributes:@{NSForegroundColorAttributeName: color}];
    _lastnameInput.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"นามสกุล" attributes:@{NSForegroundColorAttributeName: color}];
    _phoneInput.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"เบอร์โทรศัพท์" attributes:@{NSForegroundColorAttributeName: color}];
    _emailInput.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"อีเมล" attributes:@{NSForegroundColorAttributeName: color}];
}

-(IBAction)showPicker:(id)sender{
    [self.picker setHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)registerStaff:(id)sender{
    self.staffIdInput.text = [self.staffIdInput.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.firstnameInput.text = [self.firstnameInput.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.lastnameInput.text = [self.lastnameInput.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.phoneInput.text = [self.phoneInput.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.emailInput.text = [self.emailInput.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    if((self.staffIdInput.text.length>0)&&(self.firstnameInput.text.length>0)&&(self.lastnameInput.text.length>0)&&(self.phoneInput.text.length>0)&&(self.emailInput.text.length>0)&&(self.staffType.length>0)){
            self.staff = [[SBStaff alloc] initWithStaff:self.staffIdInput.text :self.firstnameInput.text :self.lastnameInput.text :self.phoneInput.text :self.emailInput.text :self.staffType :nil];
            self.staff.SBStaffDelegate = self;
            [self.staff registerStaff];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                        message: @"กรุณากรอกข้อมูลให้ครบ"
                                                       delegate: nil
                                              cancelButtonTitle: @"OK"
                                              otherButtonTitles: nil
                              ];
        [alert show];
    }
}
- (IBAction)cancle:(id)sender{
    [self dismiss];
}
- (void)dismiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)registerNoticComplete:(NSDictionary*)jsonDict{
    NSLog(@"register completed");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"สมัครสมาชิก"
                                                    message: @"เสร็จสมบูรณ์"
                                                   delegate: nil
                                          cancelButtonTitle: @"OK"
                                          otherButtonTitles: nil
                          ];
    [alert show];
    
    [self dismiss];
}
-(void)registerNoticFail:(NSDictionary*)jsonDict{
    NSLog(@"register fail");
}

#pragma mark - delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    BOOL check = YES;
    if (check) {
        [textField resignFirstResponder];
    }
    return YES;
}


- (void)highlightButton:(UIButton *)button
{
    [button setHighlighted:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
    
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.arrStatus.count;
    // return myObject.count;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    self.customerTypeLabel.text = [self.arrStatus objectAtIndex:row];
    self.staffType = [[[self.arrStatus objectAtIndex:row] lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    [self.picker setHidden:TRUE];
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = [self.arrStatus objectAtIndex:row];
   
    double version = [[[UIDevice currentDevice] systemVersion] doubleValue];
    NSAttributedString *attString;
    if(version<=6.1){
        attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    }else{
        attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    }
    
    return attString;
    
}


@end
