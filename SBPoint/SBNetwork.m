//
//  SBNetwork.m
//  SBPoint
//
//  Created by FireOneOne on 5/22/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "SBNetwork.h"
#define IP @"http://122.155.201.44/sbpointapi"

#import "AFNetworking.h"
#import "SBLocation.h"
#import "GoogleLocation.h"

@implementation SBNetwork{
    UIActivityIndicatorView *indicatorView;
    UIView *waitingView;
}
@synthesize SBNetworkDelegate;
- (NSDictionary*)addWorkLog:(NSDictionary*)data{
        NSString *method = @"workLog/addWorkLog";
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *url = [NSString stringWithFormat:@"%@/%@",IP,method];
        NSLog(@"%@",url);
        [manager POST:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"status: upload success");
            NSError *e;
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
            NSLog(@"addWorkLog result = %@",[jsonDict objectForKey:@"result"]);
           // SBLocation *workLog = [SBLocation addWorkLogId:data :jsonDict];
            SBLocation *workLog = [[SBLocation alloc] initWithDictionary:data];
            if([[jsonDict objectForKey:@"status"]isEqualToString:@"OK"]){
                [workLog addWorkLogId:jsonDict];
            }else if([[jsonDict objectForKey:@"status"]isEqualToString:@"Conflict"]){
                NSLog(@"Conflict WorkLog");
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"status: upload failed");
           // UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Oops"
//                                                            message: @"Please check connection internet."
//                                                           delegate: nil
//                                                  cancelButtonTitle: @"OK"
//                                                  otherButtonTitles: nil
//                                  ];
         //   [alert show];
            NSLog(@"Status = Not to Connect Internet");
        }];
    
    return nil;
}
- (BOOL)checkSaff:(NSDictionary*)data{
    NSString *method = @"staff/getStaffInfo";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@/%@",IP,method];
    NSLog(@"%@",url);
    [manager POST:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"status: upload success");
        NSError *e;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
        NSLog(@"check staff result = %@",[jsonDict objectForKey:@"result"]);
        if([[jsonDict objectForKey:@"status"]isEqualToString:@"OK"]){
           [SBNetworkDelegate loginComplete:jsonDict];
        }else{
            [SBNetworkDelegate loginFail];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"status: upload failed");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                        message: @"กรุณาเชื่อมต่ออินเทอร์เน็ต"
                                                       delegate: nil
                                              cancelButtonTitle: @"OK"
                                              otherButtonTitles: nil
                              ];
        [alert show];
        [SBNetworkDelegate loginFail];
    }];
    

    return true;
}
- (BOOL)checkWorkLog:(NSDictionary*)data{return true;}
- (BOOL)delWorkLog:(NSDictionary*)data{return true;}
- (BOOL)login:(NSDictionary*)data{
    [self startWaiting];
    NSString *method = @"staff/login";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@/%@",IP,method];
    NSLog(@"%@",url);
    [manager POST:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"status: upload success");

        NSError *e;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
        NSLog(@"login result = %@",[jsonDict objectForKey:@"result"]);
        if([[jsonDict objectForKey:@"status"]isEqualToString:@"OK"]){
            [self checkSaff:[jsonDict objectForKey:@"result"]];
        }else{
            NSLog(@"responseObject %@",responseObject);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                            message: [jsonDict objectForKey:@"result"]
                                                           delegate: nil
                                                  cancelButtonTitle: @"ตกลง"
                                                  otherButtonTitles: nil
                                          ];
                    [alert show];
            [SBNetworkDelegate loginFail];
        }
        [self stopWaiting];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"status: upload failed");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                        message: @"กรุณาเชื่อมต่ออินเทอร์เน็ต"
                                                       delegate: nil
                                              cancelButtonTitle: @"OK"
                                              otherButtonTitles: nil
                              ];
        [alert show];
        [SBNetworkDelegate loginFail];
        [self stopWaiting];
    }];
    
    
    return true;
}
- (BOOL)logout:(NSDictionary*)data{return true;}
- (BOOL)registerStaff:(NSDictionary*)data{
    [self startWaiting];
    NSString *method = @"staff/register";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@/%@",IP,method];
    NSLog(@"%@",url);
    [manager POST:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"status: upload success");
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Complete"
//                                                        message: [responseObject objectForKey:@"text"]
//                                                       delegate: nil
//                                              cancelButtonTitle: @"OK"
//                                              otherButtonTitles: nil
//                              ];
//        [alert show];
       
        NSError *e;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
        NSLog(@"registerStaff Key = %@",[jsonDict objectForKey:@"key"]);
        if([[jsonDict objectForKey:@"status"]isEqualToString:@"OK"]){
            [SBNetworkDelegate registerStaffComplete:jsonDict];
        }else{
            [SBNetworkDelegate registerStaffFail:jsonDict];
        }
        [self stopWaiting];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"status: upload failed");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                        message: @"กรุณาเชื่อมต่ออินเทอร์เน็ต"
                                                       delegate: nil
                                              cancelButtonTitle: @"OK"
                                              otherButtonTitles: nil
                              ];
        [alert show];
        [self stopWaiting];
    }];
    return true;
}
- (BOOL)sycnWorkLog:(NSDictionary*)data{
    NSString *method = @"workLog/addWorkLog";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@/%@",IP,method];
    NSLog(@"%@",url);
    [manager POST:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"status: upload success");
        NSError *e;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
        NSLog(@"syncWorkLog result  = %@",[jsonDict objectForKey:@"result"]);
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        SBFileOperator *operationFile = [[SBFileOperator alloc]initWithName:@"history" :@".txt" :YES];
        for(int i=0;i<[operationFile.arrayList count];i++){
            if([[operationFile.arrayList[i] objectForKey:@"key_id"]isEqualToString:[data objectForKey:@"key_id"]]){
                [dict addEntriesFromDictionary:data];
                NSDictionary *result = [responseObject objectForKey:@"result"];
                NSString *work_log_id = [NSString stringWithFormat:@"%@",[result objectForKey:@"work_log_id"]];
                [dict setObject:work_log_id forKey:@"work_log_id"];
                
                GoogleLocation *gData = [[GoogleLocation alloc]init];
                NSDictionary *address = [[NSDictionary alloc]initWithObjectsAndKeys:[dict objectForKey:@"start_lat"],@"start_lat",[dict objectForKey:@"start_lng"],@"start_lng",[dict objectForKey:@"lat"],@"lat",[dict objectForKey:@"lng"],@"lng",nil];
                [gData getDistance:address successCallback:^(id jsonResponse) {
                    NSString *distance = [NSString stringWithFormat:@"%d",(([[jsonResponse objectForKey:@"distance"] intValue])) ];
                    NSLog(@"Distance :%@",[jsonResponse objectForKey:@"distance"]);
                    [dict setObject:distance forKey:@"distance"];
//                    NSLog(@"remove  key_id : %@",[operationFile.arrayList[i] objectForKey:@"key_id"]);
                    [operationFile removeList:[operationFile.arrayList[i] objectForKey:@"key_id"]];
                    [operationFile.arrayList addObject:dict];
                    [operationFile arrayParseToJson:operationFile.arrayList];

                }errorCallback:^(NSError *error, NSString *errorMsg){
                    
                }];
                
               
            }
        }
        
       
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"status: upload failed");
        NSLog(@"Status = Not to Connect Internet");
    }];
    return NO;
}
- (void)startWaiting
{
    NSLog(@"Start Waiting");
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    waitingView = [[UIView alloc] initWithFrame:window.bounds];
    [waitingView setBackgroundColor:[UIColor blackColor]];
    [waitingView setAlpha:0.25];
    [window addSubview:waitingView];
    [window bringSubviewToFront:waitingView];
    
    indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [indicatorView setCenter:window.center];
    
    [window addSubview:indicatorView];
    [window bringSubviewToFront:indicatorView];
    [indicatorView startAnimating];
}

- (void)stopWaiting
{
    NSLog(@"Stop Waiting");
    [waitingView removeFromSuperview];
    [indicatorView removeFromSuperview];
    waitingView = nil;
    indicatorView = nil;
    
}


@end

