//
//  SBFileOperator.m
//  SBPoint
//
//  Created by FireOneOne on 5/17/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "SBFileOperator.h"

@implementation SBFileOperator{
    NSString *pathFile;
    NSArray *arrayInFile;
    BOOL isJson;
}



#pragma mark - init Object
-(id)initWithName:(NSString *) filename :(NSString *)extention :(BOOL) json{
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    pathFile = [NSString stringWithFormat:@"%@/%@%@",documentsDirectory,filename,extention];
    isJson = json;
    NSArray *oldData = [self readFileRetunArray];
    self.arrayList = [[NSMutableArray alloc]initWithArray:oldData];
    //_arrayList = [[NSMutableArray alloc]init];
    return  self;
}

-(NSString *)getPathFile{
    return pathFile;
}

-(NSDictionary *)sampleData{
    NSDictionary *tmp = [[NSDictionary alloc]initWithObjectsAndKeys:[[NSDictionary alloc] initWithObjectsAndKeys:@"100.0454",@"lat",@"34.000",@"lng", nil],@"location", nil];
    return tmp;
}

#pragma mark - file operation
-(void) readFile{
    NSString *content = [[NSString alloc] initWithContentsOfFile:pathFile
                                                    usedEncoding:nil
                                                           error:nil];
    if (content != nil) {
        arrayInFile = [self jsonToDictionary:content];
        _str = content;
    }
}

-(NSArray *) readFileRetunArray{
    if (!isJson) {
        return nil;
    }
    [self readFile];
    return arrayInFile;
}
-(NSString *) readFileRetunString{
    [self readFile];
    return _str;
}

-(void)writeFile:(NSString *)str{
    [str writeToFile:pathFile
          atomically:NO
            encoding:NSUTF8StringEncoding
               error:nil];
}
-(void)clearFile{
    [self writeFile:@""];
}

#pragma mark - convert
-(void)arrayParseToJson:(NSArray *)array{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *json = [[NSString alloc] initWithData:jsonData
                                           encoding:NSUTF8StringEncoding];
    NSLog(@"arrayParseToJson: %@",json);
    [self writeFile:json];
}
-(NSArray *)jsonToDictionary:(NSString *)strFile{
    NSData *data = [strFile dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *array = [NSJSONSerialization
                      JSONObjectWithData:data
                      options:kNilOptions
                      error:nil];
    return array;
}
-(void)removeList:(NSString *)key_id
{
    for (int i = 0 ; i<[_arrayList count]; i++) {
        if([[[_arrayList objectAtIndex:i] objectForKey:@"key_id"] isEqualToString:key_id]){
            [_arrayList removeObject:[_arrayList objectAtIndex:i]];
        }
    }
}
-(void)removeListByWorkLogId:(NSString *)work_log_id{
    for (int i = 0 ; i<[_arrayList count]; i++) {
        if([[[_arrayList objectAtIndex:i] objectForKey:@"work_log_id"] isEqualToString:work_log_id]){
            [_arrayList removeObject:[_arrayList objectAtIndex:i]];
        }
    }
}
-(NSDictionary*)getDataByKeyId:(NSString*)key_id{
    for (int i = 0 ; i<[_arrayList count]; i++) {
        if([[[_arrayList objectAtIndex:i] objectForKey:@"key_id"] isEqualToString:key_id]){
            return [_arrayList objectAtIndex:i];
        }
    }
    return nil;
}

-(void)removeFromPlaceId:(NSString *)place_id
{
    for (int i = 0 ; i<[_arrayList count]; i++) {
        if([[[_arrayList objectAtIndex:i] objectForKey:@"place_id"] isEqualToString:place_id]){
            [_arrayList removeObject:[_arrayList objectAtIndex:i]];
        }
    }
}

-(void)deleteStaffFile{
     [_arrayList removeObject:[_arrayList objectAtIndex:0]];
}
-(BOOL)hasCheckInToday{
    BOOL returnType = NO;
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"Y-MM-dd";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
  //  NSLog(@"The Current Time is %@",[dateFormatter stringFromDate:now]);
    for (int i = 0 ; i<[_arrayList count]; i++) {
        //NSLog(@"%@",[[[_arrayList objectAtIndex:i] objectForKey:@"check_in_time"] substringToIndex:10]);
        if([[[[_arrayList objectAtIndex:i] objectForKey:@"check_in_time"] substringToIndex:10] isEqualToString:[dateFormatter stringFromDate:now]]){
            returnType = YES;
        }
    }
    return returnType;
}

-(NSDictionary*)getLastStartPlaceInDay{
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"Y-MM-dd";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    int keyTmp = 0;
    for (int i = 0 ; i<[_arrayList count]; i++) {
        //NSLog(@"%@",[[[_arrayList objectAtIndex:i] objectForKey:@"check_in_time"] substringToIndex:10]);
        if([[[[_arrayList objectAtIndex:i] objectForKey:@"check_in_time"] substringToIndex:10] isEqualToString:[dateFormatter stringFromDate:now]]){
            if([[[_arrayList objectAtIndex:i] objectForKey:@"key_id"] intValue]>keyTmp){
                keyTmp = [[[_arrayList objectAtIndex:i] objectForKey:@"key_id"] intValue];
            }
        }
    }
    
    NSDictionary *tmp = [self getDataByKeyId:[NSString stringWithFormat:@"%d",keyTmp]];
    
    NSDictionary *lastStartPlaceDict = [[NSDictionary alloc]initWithObjectsAndKeys
                                        :[tmp objectForKey:@"place_name"],@"place_name",
                                        [tmp objectForKey:@"lat"],@"lat",
                                        [tmp objectForKey:@"lng"],@"lng",
                                        nil];
    
    return lastStartPlaceDict;
}
-(void)sortByKeyId{

}

@end
