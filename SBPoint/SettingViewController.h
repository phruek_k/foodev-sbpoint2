//
//  SettingViewController.h
//  SBPoint
//
//  Created by FireOneOne on 5/18/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBFileOperator.h"

#import "StartPlaceCell.h"
@interface SettingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>


//- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
@property (strong, nonatomic)NSString *place_name;
@property(strong,nonatomic) SBFileOperator *startPointFile;
- (void)refresh;
@end
