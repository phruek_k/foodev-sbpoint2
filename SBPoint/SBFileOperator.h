//
//  SBFileOperator.h
//  SBPoint
//
//  Created by FireOneOne on 5/17/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBFileOperator : NSObject

@property(nonatomic,strong) NSString *str;
@property(nonatomic,strong) NSMutableArray *arrayList;

-(id)initWithName:(NSString *) filename :(NSString *)extention :(BOOL) json;
-(NSString *)getPathFile;
-(NSDictionary *)sampleData;

-(void)readFile;
-(NSArray *) readFileRetunArray;
-(NSString *) readFileRetunString;

-(void)removeList:(NSString *)key_id;
-(void)writeFile:(NSString *)str;
-(void)arrayParseToJson:(NSArray *)array;
-(NSArray *)jsonToDictionary:(NSString *)strFile;
-(void)clearFile;
-(NSDictionary*)getDataByKeyId:(NSString*)key_id;
-(void)removeFromPlaceId:(NSString *)place_id;
-(void)deleteStaffFile;
-(BOOL)hasCheckInToday;
-(NSDictionary*)getLastStartPlaceInDay;
-(void)sortByKeyId;
-(void)removeListByWorkLogId:(NSString *)work_log_id;
@end

