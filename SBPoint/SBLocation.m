//
//  SBLocation.m
//  SBPoint
//
//  Created by FireOneOne on 5/17/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "SBLocation.h"
#import "SBNetwork.h"
#import "SBStaff.h"
#import "AFNetworking.h"
#import "GoogleLocation.h"
#define IP @"http://122.155.201.44/sbpointapi"

static int distanceFilter = 500;

@implementation SBLocation
//+ (SBLocation *)sharedInstance {
//    static dispatch_once_t onceToken;
//    static SBLocation *instance = nil;
//    dispatch_once(&onceToken, ^{
//        instance = [[SBLocation alloc] init];
//    });
//    return instance;
//}

-(id)initWithDetail:(NSString*)key_id :(NSString*)work_log_id :(NSString*)staff_id :(NSString*)place_name :(NSString*)customer_name :(NSString*)tracking_no :(NSString*)start_place :(NSString*)start_lat :(NSString*)start_lng :(NSString*)lat :(NSString*)lng :(NSString*)check_in_time :(NSString*)check_out_time :(NSString*)address :(NSString*)working_time :(NSString*)status :(NSString*)work_log_type :(NSString*)distance {
    
    self.dict = [[NSDictionary alloc]initWithObjectsAndKeys:key_id,@"key_id",work_log_id,@"work_log_id",staff_id,@"staff_id",place_name,@"place_name",customer_name,@"customer_name",tracking_no,@"tracking_no",start_place,@"start_place",start_lat,@"start_lat",start_lng,@"start_lng",lat,@"lat",lng,@"lng",check_in_time,@"check_in_time",check_out_time,@"check_out_time",address,@"address",working_time,@"working_time",status,@"status",work_log_type,@"work_log_type",distance,@"distance",nil];
    
    return self;
}
-(id)initWithDictionary:(NSDictionary*)data{
    self.dict = data;
    return self;
}
-(NSDictionary*)getDetail{
    return self.dict;
}
-(void)checkOut{
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    _current_key_id = [f numberFromString:[[NSUserDefaults standardUserDefaults] stringForKey:@"current_key_id"]];
    [self getNowLocation];
}

- (void)getNowLocation{
    NSLog(@"getNowLocation");
    if(_locations==nil){
        _locations = [[NSMutableArray alloc] init];
    }
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //self.locationManager.distanceFilter = 1;
    [self.locationManager setDelegate:self];
    [self.locationManager startUpdatingLocation];

}

- (void)stopUpdateLocation{
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
   
    
    if (UIApplication.sharedApplication.applicationState == UIApplicationStateActive){
       // NSLog(@"App is foreground. New location is %@", newLocation);
    }else{
     //   NSLog(@"App is backgrounded. New location is %@", newLocation);
    }
    [_locations addObject:newLocation];
    
  //  int countLog = [_locations count];
    CLLocation *startPoint = [_locations objectAtIndex:0];
    CLLocation *lastPoint = newLocation;
    NSLog(@"Distance %d",[self Callocation:startPoint point2:lastPoint]);
    
    NSLog(@"First time stamp : %@",startPoint.timestamp);
    NSLog(@"Last time stamp : %@",lastPoint.timestamp);
    
    
    NSTimeInterval timeDifference = [lastPoint.timestamp timeIntervalSinceDate:startPoint.timestamp];
    NSLog(@"timeDifference %f",timeDifference);
    
    
    if([self Callocation:startPoint point2:lastPoint]>=distanceFilter||timeDifference>=7200){
        NSLog(@"Over %d let's check out",distanceFilter);
        [self.locationManager stopUpdatingLocation];
        [self saveCheckOut];
    }
}



-(int)Callocation:(CLLocation *)point1
           point2:(CLLocation *)point2{
    CLLocationDistance distance = [point1 distanceFromLocation:point2];
    return distance;
}

-(void)saveCheckOut{
    _operationFile = [[SBFileOperator alloc]initWithName:@"history" :@".txt" :YES];
    NSLog(@"Save Check out ,all locations in file : %d, current_key_id : %@",(int)[_operationFile.arrayList count],[NSString stringWithFormat:@"%d",[_current_key_id intValue]]);
    NSDate *now = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"Y-M-dd HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSLog(@"now current_key_id : %@",[NSString stringWithFormat:@"%d",[_current_key_id intValue]]);
    

    for (int i = 0 ; i<[_operationFile.arrayList count]; i++) {
        
        if([[_operationFile.arrayList[i] objectForKey:@"key_id"] isEqualToString:[NSString stringWithFormat:@"%d",[_current_key_id intValue]]]){
            NSLog(@"in loop %d",[_current_key_id intValue]);
            
            CLLocation *startLocation = [[CLLocation alloc]initWithLatitude
                                         :[[_operationFile.arrayList[i] objectForKey:@"start_lat"] doubleValue] longitude:[[_operationFile.arrayList[i] objectForKey:@"start_lng"] doubleValue]];
            
            CLLocation *endLocation = [[CLLocation alloc]initWithLatitude:
                                       [[_operationFile.arrayList[i] objectForKey:@"lat"] doubleValue] longitude:[[_operationFile.arrayList[i] objectForKey:@"lng"] doubleValue]];
            
            int totalDistance = [self Callocation:startLocation point2:endLocation];
            
            SBLocation *checkOutePoint = [[SBLocation alloc]initWithDetail
                                          :[_operationFile.arrayList[i] objectForKey:@"key_id"]
                                          :[_operationFile.arrayList[i] objectForKey:@"work_log_id"]
                                          :[_operationFile.arrayList[i] objectForKey:@"staff_id"]
                                          :[_operationFile.arrayList[i] objectForKey:@"place_name"]
                                          :[_operationFile.arrayList[i] objectForKey:@"customer_name"]
                                          :[_operationFile.arrayList[i] objectForKey:@"tracking_no"]
                                          :[_operationFile.arrayList[i] objectForKey:@"start_place"]
                                          :[_operationFile.arrayList[i] objectForKey:@"start_lat"]
                                          :[_operationFile.arrayList[i] objectForKey:@"start_lng"]
                                          :[_operationFile.arrayList[i] objectForKey:@"lat"]
                                          :[_operationFile.arrayList[i] objectForKey:@"lng"]
                                          :[_operationFile.arrayList[i] objectForKey:@"check_in_time"]
                                          :[dateFormatter stringFromDate:now]
                                          :[_operationFile.arrayList[i] objectForKey:@"address"]
                                          :[_operationFile.arrayList[i] objectForKey:@"working_time"]
                                          :[_operationFile.arrayList[i] objectForKey:@"status"]
                                          :[_operationFile.arrayList[i] objectForKey:@"work_log_type"]
                                          :[NSString stringWithFormat:@"%d",totalDistance]
                                          ];

           // [self syncLocationToServer:[checkOutePoint getDetail]];
            [_operationFile removeList:[_current_key_id stringValue]];
            [_operationFile.arrayList addObject:checkOutePoint.dict];
            [_operationFile arrayParseToJson:_operationFile.arrayList];
        }
    }
}

-(BOOL)directCheckOut{
    NSLog(@"Direct checkout");
    _operationFile = [[SBFileOperator alloc]initWithName:@"history" :@".txt" :YES];
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    _current_key_id = [f numberFromString:[[NSUserDefaults standardUserDefaults] stringForKey:@"current_key_id"]];

    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"Y-M-dd HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSLog(@"now current_key_id : %@",[NSString stringWithFormat:@"%d",[_current_key_id intValue]]);
    
    for (int i = 0 ; i<[_operationFile.arrayList count]; i++) {
       
        if([[_operationFile.arrayList[i] objectForKey:@"key_id"] isEqualToString:[NSString stringWithFormat:@"%d",[_current_key_id intValue]]]){
            NSLog(@"check out %@",[_operationFile.arrayList[i] objectForKey:@"check_out_time"]);
            if([[_operationFile.arrayList[i] objectForKey:@"check_out_time"] isEqualToString:@"check_out_time"]){
                [self.locationManager stopUpdatingLocation];
                NSLog(@"directCheckOut Check out ,all locations in file : %d, current_key_id : %@",(int)[_operationFile.arrayList count],[NSString stringWithFormat:@"%d",[_current_key_id intValue]]);
                for (int i = 0 ; i<[_operationFile.arrayList count]; i++) {
                    
                    if([[_operationFile.arrayList[i] objectForKey:@"key_id"] isEqualToString:[NSString stringWithFormat:@"%d",[_current_key_id intValue]]]){
                        NSLog(@"in loop %d",[_current_key_id intValue]);
                      
                        //find distance
                        CLLocation *startLocation = [[CLLocation alloc]initWithLatitude
                        :[[_operationFile.arrayList[i] objectForKey:@"start_lat"] doubleValue] longitude:[[_operationFile.arrayList[i] objectForKey:@"start_lng"] doubleValue]];
                        
                        CLLocation *endLocation = [[CLLocation alloc]initWithLatitude:
                        [[_operationFile.arrayList[i] objectForKey:@"lat"] doubleValue] longitude:[[_operationFile.arrayList[i] objectForKey:@"lng"] doubleValue]];
                        
                        int totalDistance = [self Callocation:startLocation point2:endLocation];
                        
                        SBLocation *checkOutePoint = [[SBLocation alloc]initWithDetail
                                                    :[_operationFile.arrayList[i] objectForKey:@"key_id"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"work_log_id"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"staff_id"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"place_name"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"customer_name"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"tracking_no"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"start_place"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"start_lat"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"start_lng"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"lat"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"lng"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"check_in_time"]
                                                    :[dateFormatter stringFromDate:now]
                                                    :[_operationFile.arrayList[i] objectForKey:@"address"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"working_time"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"status"]
                                                    :[_operationFile.arrayList[i] objectForKey:@"work_log_type"]
                                                    :[NSString stringWithFormat:@"%d",totalDistance]];
                       
                       // [self syncLocationToServer:[checkOutePoint getDetail]];
                        [_operationFile removeList:[_current_key_id stringValue]];
                        [_operationFile.arrayList addObject:checkOutePoint.dict];
                        [_operationFile arrayParseToJson:_operationFile.arrayList];
                        }
                }
            }
        }
    }
    return YES;
}
-(void)syncLocationToServer:(NSDictionary*)locationData{
    SBNetwork *locationSync = [[SBNetwork alloc]init];
    NSDictionary *workLog = [locationSync addWorkLog:locationData];
}
-(void)setLocationDic:(NSMutableDictionary *)data{
    _dict = data;
}

-(void)addWorkLogId:(NSDictionary*)response{
    _operationFile = [[SBFileOperator alloc]initWithName:@"history" :@".txt" :YES];
    NSMutableDictionary *updateWorkLog = [[NSMutableDictionary alloc] init];
    [updateWorkLog addEntriesFromDictionary:_dict];
    NSString *work_log_id = [NSString stringWithFormat:@"%@",[[response objectForKey:@"result"] objectForKey:@"work_log_id"]];
    
   
    [updateWorkLog setObject:work_log_id forKey:@"work_log_id"];
    [self setLocationDic:updateWorkLog];
    [_operationFile removeList:[_dict objectForKey:@"key_id"]];
    [_operationFile.arrayList addObject:_dict];
    [_operationFile arrayParseToJson:_operationFile.arrayList];
}

-(void)deleteWorkLog:(int)key_id{
    _operationFile = [[SBFileOperator alloc]initWithName:@"history" :@".txt" :YES];
    [_operationFile removeList:[NSString stringWithFormat:@"%d",key_id]];
}
-(void)checkLocationToSync{
    SBStaff *staffData = [[SBStaff alloc] init];
   // NSDictionary *data = [[NSDictionary alloc]initWithObjectsAndKeys:[staffData getStaffId],@"staff_id", nil];
    NSDictionary *data = [staffData getStaff];
    //SBNetwork *staffSync = [[SBNetwork alloc]init];
    NSString *method = @"staff/getStaffInfo";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@/%@",IP,method];
    //NSLog(@"%@",url);
    [manager POST:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
      //  NSLog(@"status: upload success");
        
        NSError *e;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
        NSLog(@"Check staff result = %@",[jsonDict objectForKey:@"result"]);
        if([[jsonDict objectForKey:@"status"]isEqualToString:@"OK"]){
            NSString *serverKey = [NSString stringWithFormat:@"%@",[[jsonDict objectForKey:@"result"] objectForKey:@"key"] ];
            if(!([serverKey isEqualToString:[data objectForKey:@"key"]])||serverKey.length==0){
                SBStaff *staff = [[SBStaff alloc]init];
               [staff clearStaffInformation];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"backToLoginOnCheckIn" object:self userInfo:nil];
                [self clearAllLocation];
            }else{
                [self syncLocation];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"status: upload failed");
    }];

}
-(void)syncLocation{
    _operationFile = [[SBFileOperator alloc]initWithName:@"history" :@".txt" :YES];
    NSMutableArray *tmpArrayToSync = [[NSMutableArray alloc]init];
    for(int i=0;i<[_operationFile.arrayList count];i++){
        if([[_operationFile.arrayList[i] objectForKey:@"work_log_id"]isEqualToString:@"null"]&&![[_operationFile.arrayList[i] objectForKey:@"check_out_time"]isEqualToString:@"check_out_time"]){
            [tmpArrayToSync addObject:_operationFile.arrayList[i]];
        }
    }
    SBNetwork *syncFile = [[SBNetwork alloc]init];
    NSLog(@"\n Sync to Sever total %d",(int)[tmpArrayToSync count]);
    for(int c=0;c<[tmpArrayToSync count];c++){
        BOOL isSuccess = [syncFile sycnWorkLog:tmpArrayToSync[c]];
        if(isSuccess){
            NSLog(@"\n Sync key_id %@",[tmpArrayToSync[c] objectForKey:@"key_id"]);
        }else{
            NSLog(@"\n Fail Sync key_id %@",[tmpArrayToSync[c] objectForKey:@"key_id"]);
        }
    }

}

-(void)clearAllLocation{
    SBFileOperator *staffFile = [[SBFileOperator alloc]initWithName:@"history":@".txt" :YES];
    [staffFile.arrayList removeAllObjects];
    [staffFile arrayParseToJson:staffFile.arrayList];
}

@end
