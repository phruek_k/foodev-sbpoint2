//
//  LoginViewController.m
//  SBPoint
//
//  Created by FireOneOne on 5/23/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
@property(strong,nonatomic)NSString* staff_id;
@property (weak, nonatomic) IBOutlet UITextField *staffIdInput;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UIView *loginInputView;
@property (weak, nonatomic) IBOutlet UIView *buttonView;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation LoginViewController
int coY;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _loginInputView.alpha = 0.0;
    _buttonView.alpha = 0.0;
     NSLog(@"viewDidLoad y1 : %d",(int)_logoImage.frame.origin.y);
    [self initStyle];
   
   
}

- (void)initStyle{
    _staffIdInput.font = [UIFont fontWithName:@"DBHelvethaicaX-Li" size:18];
    _loginButton.titleLabel.font = [UIFont fontWithName:@"DBHelvethaicaX-Med" size:20];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    if(coY!=145){
//        CGRect rect = self.logoImage.frame;
//        
//        self.logoImage.layer.frame = CGRectMake(rect.origin.x, 145, rect.size.width, rect.size.height);
//        
        [UIView animateWithDuration:0.9 animations:^{
//            CGRect rect = self.logoImage.frame;
//            
//            self.logoImage.layer.frame = CGRectMake(rect.origin.x, 145, rect.size.width, rect.size.height);
        } completion:^(BOOL finished) {
//            [UIView beginAnimations: @"Fade In" context:nil];
//            [UIView setAnimationDelay:0.2];
//            [UIView setAnimationDuration:0.2];
                _loginInputView.alpha = 1;
                _buttonView.alpha = 1;
//            [UIView commitAnimations];
//            coY = (int)(_logoImage.layer.frame.origin.y);
        }];
   // }
    
   
    
    
}
-(IBAction)login:(id)sender{
    _staff_id = [_staffIdInput.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(_staff_id.length<=0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"คำเตือน"
                                                        message: @"กรุณากรอกรหัสพนักงาน"
                                                       delegate: nil
                                              cancelButtonTitle: @"ตกลง"
                                              otherButtonTitles: nil
                                                            ];
        [alert show];
    }else{
        SBStaff *staff = [[SBStaff alloc]initWithStaff:_staff_id :nil :nil :nil :nil :nil :nil];
        [staff login];
    }
}


-(void)loginComplete{
   NSLog(@"loginComplete");
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"Navigate"];
    [[[[UIApplication sharedApplication] delegate] window] setRootViewController:vc];
    coY=0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    BOOL check = YES;
    if (check) {
        [textField resignFirstResponder];
    }
    return YES;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"RegisterViewController"]) {
        NSLog(@"=== RegisterViewController ===");
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
