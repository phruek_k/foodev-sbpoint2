//
//  SBLocation.h
//  SBPoint
//
//  Created by FireOneOne on 5/17/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "SBFileOperator.h"
@interface SBLocation : NSObject<CLLocationManagerDelegate>{
//    NSDictionary* _dict;
//    CLLocationManager * _locationManager;
//    NSMutableArray * _locations;
//    SBFileOperator * _operationFile;
//    NSNumber * _current_key_id;
}
- (void)clearAllLocation;
- (int)Callocation:(CLLocation *)point1
           point2:(CLLocation *)point2;
- (id)initWithDetail:(NSString*)key_id :(NSString*)work_log_id :(NSString*)staff_id :(NSString*)place_name :(NSString*)customer_name :(NSString*)tracking_no :(NSString*)start_place :(NSString*)start_lat :(NSString*)start_lng :(NSString*)lat :(NSString*)lng :(NSString*)check_in_time :(NSString*)check_out_time :(NSString*)address :(NSString*)working_time :(NSString*)status :(NSString*)work_log_type :(NSString*)distance;
- (void)checkOut;
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation;
@property(strong,nonatomic)NSDictionary* dict;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableArray *locations;
@property(nonatomic,strong) SBFileOperator *operationFile;
@property (strong,nonatomic) NSNumber *current_key_id;
- (BOOL)directCheckOut;
- (void)syncLocationToServer:(NSDictionary*)locationData;
- (void)addWorkLogId:(NSDictionary*)data;
- (id)initWithDictionary:(NSDictionary*)data;
- (void)deleteWorkLog:(int)key_id;
- (void)checkLocationToSync;
- (void)stopUpdateLocation;

@end
