//
//  MHTabBarSegue.m
//  SBPoint
//
//  Created by FireOneOne on 5/20/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "MHTabBarSegue.h"
#import "ConfigViewController.h"
@implementation MHTabBarSegue
- (void) perform {
    ConfigViewController *tabBarViewController = (ConfigViewController *)self.sourceViewController;
    UIViewController *destinationViewController = (UIViewController *) tabBarViewController.destinationViewController;
    
    //remove old viewController
    if (tabBarViewController.oldViewController) {
        [tabBarViewController.oldViewController willMoveToParentViewController:nil];
        [tabBarViewController.oldViewController.view removeFromSuperview];
        [tabBarViewController.oldViewController removeFromParentViewController];
    }
    
    
    destinationViewController.view.frame = tabBarViewController.container.bounds;
    [tabBarViewController addChildViewController:destinationViewController];
    [tabBarViewController.container addSubview:destinationViewController.view];
    [destinationViewController didMoveToParentViewController:tabBarViewController];
}
@end
