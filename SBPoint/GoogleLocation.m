//
//  GoogleLocation.m
//  com.voize.SBPoint
//
//  Created by FireOneOne on 5/28/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import "GoogleLocation.h"
#import "AFNetworking.h"
@implementation GoogleLocation
-(void)getLocationFromCLlocation:(CLLocation*)location{
    double lat = location.coordinate.latitude;
    double lng = location.coordinate.longitude;
    NSString *link = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=false&language=th",lat,lng];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@",link];
    NSLog(@"%@",url);
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSError *e;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
        if([[jsonDict objectForKey:@"status"] isEqualToString:@"OK"]){
        NSArray *result = [[NSArray alloc]initWithArray:[jsonDict objectForKey:@"results"]];
        NSArray *address_components = [[NSArray alloc] initWithArray:[[result objectAtIndex:0] objectForKey:@"address_components"]];
        //NSLog(@"result = %@",[[address_components objectAtIndex:3] objectForKey:@"short_name"]);
        NSString *tumbol = [[address_components objectAtIndex:2] objectForKey:@"short_name"];
        NSString *aumpur = [[address_components objectAtIndex:3] objectForKey:@"short_name"];
        NSString *province = [[address_components objectAtIndex:4] objectForKey:@"short_name"];
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:tumbol,@"tumbol",aumpur,@"aumpur",province,@"province",nil];
        [self.Gdelegate setLocationAddress:dic];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, id responseObject){

        
    }];
}

-(void)getDistance:(NSDictionary *) params
   successCallback:(void (^)(id jsonResponse)) successCallback
     errorCallback:(void (^)(NSError * error, NSString *errorMsg)) errorCallback{
  //  NSString *urlTest = @" ";
    NSString *urlStr =[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%@,%@&destination=%@,%@",[params objectForKey:@"start_lat"],[params objectForKey:@"start_lng"],[params objectForKey:@"lat"],[params objectForKey:@"lng"]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:urlStr parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Status: upload success");
        NSError *e;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
        NSString *distance = [[[[[[jsonDict objectForKey:@"routes"] objectAtIndex:0] objectForKey:@"legs"] objectAtIndex:0]objectForKey:@"distance"]objectForKey:@"value"];
        NSDictionary *distanceDict = [[NSDictionary alloc]initWithObjectsAndKeys:distance,@"distance", nil];
        successCallback(distanceDict);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        errorCallback(error, nil);
    }];

}

@end
