//
//  GoogleLocation.h
//  com.voize.SBPoint
//
//  Created by FireOneOne on 5/28/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol GoogleLocationDelegate <NSObject>
-(void)setLocationAddress:(NSDictionary*)locationAddress;
@end

@interface GoogleLocation : NSObject
@property (strong,nonatomic)id <GoogleLocationDelegate>Gdelegate;
-(void)getLocationFromCLlocation:(CLLocation*)location;
-(void)getDistance:(NSDictionary *) params
           successCallback:(void (^)(id jsonResponse)) successCallback
             errorCallback:(void (^)(NSError * error, NSString *errorMsg)) errorCallback;
@end
