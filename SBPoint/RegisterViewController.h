//
//  RegisterViewController.h
//  SBPoint
//
//  Created by FireOneOne on 5/23/14.
//  Copyright (c) 2014 FireOneOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *staffIdInput;
@property (weak, nonatomic) IBOutlet UITextField *firstnameInput;
@property (weak, nonatomic) IBOutlet UITextField *lastnameInput;
@property (weak, nonatomic) IBOutlet UITextField *phoneInput;
@property (weak, nonatomic) IBOutlet UITextField *emailInput;
-(void)registerNoticComplete:(NSDictionary*)jsonDict;
-(void)registerNoticFail:(NSDictionary*)jsonDict;
@end
